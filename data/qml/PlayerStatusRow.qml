import QtQuick 2.0

Row {
    property var player
    property var current: turnPlayer(turnsBefore) == player.color
    spacing: 10

    Text {
        color: "black"
        text: player.points + "P"
        font.bold: true
    }

    Row {
        spacing: 1
        Repeater {
            model: player.penguins
            Penguin {
                opacity: modelData.position == null ? 1 : 0.2
                width: name.height
                height: name.height
                color: player.color
                simple: true                
            }
        }
    }
    Text {
        id: name
        color: current ? Qt.darker(player.color) : player.color
        font.underline: current
        text: player.name
    }

}