import QtQuick 2.0

Item {
    id: floe
    property var diameter : 32
    property var model
    height: diameter;
    width: Math.sqrt(3) / 2 * diameter + 2;
    opacity: model.fishCount == 0 ? 0.4 : 1

    Canvas {
        id: canvas
        anchors.fill: parent

        property color fillColor :
          model.fishCount == 1 ? "#6bffd3" :
          model.fishCount == 2 ? "#00f0a8" :
          model.fishCount == 3 ? "#00b37d" :
          "lightgrey"
	opacity: 0.8
        property var strokeColor : model.fishCount == 0 ? "palegoldenrod" : "black"

        Connections {
            target: modelData
            onFishCountChanged: canvas.requestPaint()
        }

  	Connections {
	    target: floe
            onDiameterChanged: canvas.requestPaint()
	}

        onPaint: {
            var ctx = canvas.getContext("2d");
            var halfwidth = floe.diameter * Math.sqrt(3) / 4;
            ctx.save();
            
            ctx.fillStyle = canvas.fillColor;
            ctx.strokeStyle = canvas.strokeColor;
            ctx.beginPath();
            ctx.moveTo(1, floe.diameter / 4);
            ctx.lineTo(1, floe.diameter / 4 * 3);
            ctx.lineTo(halfwidth, floe.diameter);
            ctx.lineTo(halfwidth * 2, floe.diameter / 4 * 3);
            ctx.lineTo(halfwidth * 2, floe.diameter / 4);
            ctx.lineTo(halfwidth, 0);
            ctx.closePath();
            ctx.stroke();
            ctx.fill();

            ctx.restore();
        }
    }

    Text {
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        text: model.boardIndex
 	property var both: model.reachable.red && model.reachable.blue;
	property var red: model.reachable.red;
	property var blue: model.reachable.blue;
	font.weight: Font.Normal
	font.pixelSize: 0.4 * (parent.width / 2)
	color:
          both ? "black" :
          red  ? "red" :
          blue ? "blue" :
          "black"
    }
}