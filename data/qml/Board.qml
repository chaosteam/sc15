import QtQuick 2.0

Item {
    id: page
    property var maximumWidth
    property var maximumHeight
    property var diameter : 
      Math.min(2 * ((maximumWidth / 8) / Math.sqrt(3)), maximumHeight / 25 * 4)
    property var widthHeightRatio: (8 * Math.sqrt(3) / 2) / (25 / 4)

    width: tileWidth * 8
    height: diameter * 25 / 4

    property var tileWidth : diameter * Math.sqrt(3) / 2
    property var tileHeight : diameter
    property var model

    function toTilePosition(pos) {
        var start = (pos.row % 2 == 0) ? page.tileWidth / 2 : 0;
        var result = {
            y: pos.row * page.tileHeight * 3/4, 
            x: pos.column * page.tileWidth + start
        };
        return result;
    }

    function fromTilePosition(pos) {
        var row = pos.y / page.tileHeight * 4/3
        var start = (pos.row % 2 == 0) ? page.tileWidth / 2 : 0;
        return { row: row, column: (pos.x - start) / page.tileWidth }
    }

    property var selected: []

    Repeater {
        id: hexgrid
        model: page.model
        Repeater { 
            id: row
            property var even: index % 2 == 0
            property var rowIndex: index
            model: modelData.floes
            Floe {
                id: floe
                diameter: page.diameter - 2;
                property var tilePos: toTilePosition({row: rowIndex, column: index})
                x: tilePos.x;
                y: tilePos.y;
                model: modelData
            } 
        }
    }
}