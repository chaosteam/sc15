import QtQuick 2.0

Canvas {
    id: canvas
    property var from
    property var to
    property var tileWidth
    property var color
    property var num

    onFromChanged: requestPaint()
    onToChanged: requestPaint()
    
    onPaint: {
        if(from == null && to == null) return;
        var ctx = canvas.getContext("2d");
        ctx.save();

        ctx.beginPath();
        ctx.lineWidth = 3;
        ctx.strokeStyle = Qt.darker(canvas.color);
        ctx.font = "12px sans-serif";
        if(from != null && to != null) {
            ctx.beginPath();
            ctx.clearRect(0,0, width, height);
            var dx = to.x - from.x;
            var dy = to.y - from.y;
            var norm = Math.sqrt(dx * dx + dy * dy);
            dx /= norm;
            dy /= norm;
            var orthoX = -dy;
            var orthoY = dx;

            to.x -= 5 * dx;
            to.y -= 5 * dy;
            
            ctx.moveTo(from.x, from.y);
            ctx.lineTo(to.x, to.y);
            ctx.lineTo(to.x + orthoX * 3, to.y + orthoY * 3);
            ctx.lineTo(to.x + 5*dx, to.y + 5*dy);
            ctx.lineTo(to.x - orthoX * 3, to.y - orthoY * 3);
            ctx.lineTo(to.x, to.y);
            ctx.stroke();
            ctx.lineWidth = 1;
            ctx.text(num, (from.x + to.x) / 2 + 15, (from.y + to.y) / 2 + 15);
        } else {
            var pos = from ? from : to;
            var d = tileWidth / 4
            ctx.moveTo(pos.x + d, pos.y + d);
            ctx.lineTo(pos.x - d, pos.y - d);
            ctx.moveTo(pos.x + d, pos.y - d);
            ctx.lineTo(pos.x - d, pos.y + d);
            ctx.stroke();
            ctx.textBaseline = "middle";
            ctx.lineWidth = 1;
            ctx.text(num, pos.x + 15, pos.y);
        }
        ctx.stroke();

        ctx.restore();
    }
}