import QtQuick 2.0
import QtQuick.Layouts 1.1
import QtQuick.Window 2.2
import QtQuick.Controls 1.2

Window {
    id: window
    color: "lightgray"
    title: "Hey that's my fish! Player"

    property var wideView : window.width - game.width > window.height - game.height

    Row {
        id: statusBottom
        height: childrenRect.height
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10
        spacing: 5

        BusyIndicator {
            running: working
            height: statusText.height
            width: statusText.height
        }

        Text {
            id: statusText
            text: "Search depth: " + depth + " | Turns: " + turnsBefore + "/60" + " | Auto: " + autoCompute + " | " + (possibility + 1)
        }
    }

    Game {
        id: game
        maximumWidth: window.width - 20
        maximumHeight: window.height - statusBottom.height - statusTop.childrenRect.height - 20
        anchors.top: statusTop.bottom
        anchors.left: parent.left
        anchors.margins: 10
        anchors.topMargin: wideView ? 0 : 20
    }

    Item {

        id: statusTop
        visible: !wideView
        anchors.margins: 10
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.top: parent.top
        height: wideView ? 0 : childrenRect.height

        PlayerStatusRow {
            player: players[0]
            anchors.left: parent.left
            anchors.top: parent.top
        }

        Text {
            height: parent.height
            text: "VS"
            color: "black"
            anchors.left: parent.left
            anchors.right: parent.right
            font.bold: true
            horizontalAlignment: Text.AlignHCenter
        }

        PlayerStatusRow {
            player: players[1]
            anchors.right: parent.right
            anchors.top: parent.top
            layoutDirection: Qt.RightToLeft
        }
    }

    Item {

        id: statusSide
        anchors.left: game.right
        anchors.top: game.top
        anchors.bottom: game.bottom
        anchors.leftMargin: 50
        visible: wideView

        PlayerStatusSide {
            id: statusSideRed
            player: players[0]
            anchors.top: parent.top
            height: childrenRect.height
        }

        PlayerStatusSide {
            id: statusSideBlue
            player: players[1]
            anchors.top: statusSideRed.bottom
            anchors.topMargin: 20
            height: parent.height / 4
            x: 0
        }


        Column {
            anchors.bottom: parent.bottom
            CheckBox {
                id: humanRed
                text: "Human plays red"
            }

            Button {
                text: "New game"
         
                onClicked: {
                    bot["red"] = false;
                    bot["blue"] = false;
                    newGame(humanRed.checked);
                    var botPlayer = humanRed.checked ? "blue" : "red";
                    bot[botPlayer] = true;
                    if(!humanRed.checked) performTurn(2000000);
                }
            }
        }

   }

   property var autoCompute : false
   property var turnAlias : game.forwardTurn
   property var moveIndex : 0
   property var bot : { "red" : false, "blue" : true };

   Component.onCompleted: window.show()


   onTurnAliasChanged: {
     if(bot && bot[turnPlayer(game.forwardTurn)]) {
       performTurn(2000000);
     }
     else if(autoCompute) { 
       updateMoves(); 
       moveIndex = 0; 
     } 
   }
   
   Item {
        anchors.fill: parent
        Keys.enabled: true
        focus: true
        Keys.onUpPressed: { stopWorker(); depth += 1; moveIndex = 0 }
        Keys.onDownPressed: { stopWorker(); depth -= 1; moveIndex = 0 }
        Keys.onEscapePressed: stopWorker()
        Keys.onSpacePressed: performTurn(2000000)
        Keys.onReturnPressed: { updateMoves(); moveIndex = 0 }
        Keys.onPressed: {
            if(event.key == Qt.Key_A) autoCompute = !autoCompute;
            if(event.key == Qt.Key_M) {
                if(moves.length == 0) return;
                var from = moves[moveIndex].from ? fromCoords(moves[moveIndex].from.row, moves[moveIndex].from.column) : null;
                var to   = moves[moveIndex].to ? fromCoords(moves[moveIndex].to.row, moves[moveIndex].to.column) : null;
                if(doMove(from, to)) moveIndex++;
            }
            if(event.key == Qt.Key_P) {
                back();
                moveIndex--;
            }
            if(event.key == Qt.Key_R) {
                stopWorker(); depth = 0;
            }
	    if(event.key == Qt.Key_J) previousPV();
	    if(event.key == Qt.Key_K) nextPV();
	    if(event.key == Qt.Key_T) performTurn(2000000);
	    if(event.key == Qt.Key_B) {
	      bot[turnPlayer(game.forwardTurn)] = !bot[turnPlayer(game.forwardTurn)];
	      if(bot[turnPlayer(game.forwardTurn)]) performTurn(2000000);
	    }
        }
    }
}