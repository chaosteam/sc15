import QtQuick 2.0

Item {
    property var player
    property var current: turnPlayer(turnsBefore) == player.color

    Rectangle {
        id: indicator
        anchors.right: name.left
        width: 10
        height: 10
        anchors.verticalCenter: name.verticalCenter
        anchors.rightMargin: 10
        color: player.color
        opacity: 0

        states: State {
            name: "current"
            when: current
            PropertyChanges { target: indicator; opacity: 1 }
        }

        transitions: Transition {
            to: "current"; reversible: true
            PropertyAnimation { property: "opacity"; duration: 100; }
        }
    }

    Text {
        id: name
        color: player.color
        text: player.name
        font.pointSize: 20
    }

    Text {
        id: points
        anchors.top: name.bottom
        anchors.topMargin: 10
        font.pointSize: 15
        text: player.points + " points"
    }

    Row {
        anchors.top: points.bottom
        anchors.topMargin: 10
        spacing: -10
        Repeater {
            model: player.penguins
            Penguin {
                opacity: modelData.position == null ? 1 : 0.2
                width: 32
                height: 32
                color: player.color
            }
        }
    }

    
}