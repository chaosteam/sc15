import QtQuick 2.0

Canvas {
  id: canvas
  property var color;
  property var simple : false;
  antialiasing: true
  onWidthChanged: requestPaint()
  onHeightChanged: requestPaint()
  onPaint: {
      var ctx = canvas.getContext("2d");
      ctx.save();

      ctx.beginPath();                
      ctx.arc(canvas.width / 2, canvas.height / 2, Math.min(canvas.height / 2, canvas.width / 2) - (simple ? 1 : 4), 0, 2 * Math.PI, false);
      if(!simple) {
          ctx.strokeStyle = "white";
          ctx.lineWidth = 8;
          ctx.stroke();
          ctx.lineWidth = 6;
      }
      ctx.strokeStyle = color;
      ctx.stroke();
      if(!simple) {
          ctx.strokeStyle = Qt.darker(color);
          ctx.lineWidth = 1;
          ctx.stroke();
      }

      ctx.restore();
  }
}
