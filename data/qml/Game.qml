import QtQuick 2.0

Item {

    property var maximumWidth
    property var maximumHeight

    Board {
        id: boardView
        model: board
        maximumWidth: parent.maximumWidth
        maximumHeight: parent.maximumHeight
    }

    width: boardView.width
    height: boardView.height
    property var currentMoveFrom
    property var currentMovePossible

    property var forwardTurn: turnsBefore 
    

    Repeater {
        id: currentMove
        model: turnsBefore <= 7 ? setPositions(players[forwardTurn % 2].penguins) : currentMovePossible

        function setPositions(penguins) {
            var positions = [];
            penguins.forEach(function(penguin) {
                if(penguin.position == null) positions = penguin.reachable;
            });
            return positions 
        }

        Item {
            z: 5
            y: tile.y + 0.125 * boardView.tileHeight + 3
            x: tile.x
            height: 0.75 * boardView.tileWidth
            width:  boardView.tileWidth
            property var tile: boardView.toTilePosition(modelData)

            Penguin {
                color: turnPlayer(forwardTurn)
                opacity: 0.4
                visible: mouse.containsMouse || turnsBefore > 7
                anchors.fill: parent
            }

            MouseArea {
                id: mouse
                hoverEnabled: turnsBefore <= 7
                anchors.fill: parent
                enabled: true
                onClicked: {
                    var fp = forwardTurn <= 7 ? null : fromCoords(currentMoveFrom.row, currentMoveFrom.column);
                    var tp = fromCoords(modelData.row, modelData.column);
                    doMove(fp, tp);
                    currentMovePossible = [];
                }
            }
        }
    }

    
    Repeater {
        model: players

        Repeater {
            id: player
            model: modelData.penguins
            property var color: modelData.color

            Penguin {
                color: player.color
                visible: modelData.position != null

                function penguinPos() {
                    return boardView.toTilePosition(modelData.position);
                }

                x: modelData.position == null ? boardView.width : penguinPos().x
                y: modelData.position == null ? 0 : penguinPos().y
                width: boardView.tileWidth
                height: boardView.tileHeight

                Behavior on x { NumberAnimation { duration: 100; } }
                Behavior on y { NumberAnimation { duration: 100; } }

                MouseArea {
                    anchors.fill: parent
                    enabled: player.color == turnPlayer(forwardTurn) && forwardTurn > 7
                    onClicked: {
                        currentMovePossible = modelData.reachable;
                        currentMoveFrom = modelData.position;
                    }
                }

            }

        }
    }

    function toTileCenter(pos) {
        var tilePos = boardView.toTilePosition(pos);
        return {
            x: tilePos.x + boardView.tileWidth / 2, 
            y: tilePos.y + boardView.diameter / 2
        };
    }

    Repeater {
        model: moves

        Move {
            opacity: 0.9
            z: 10
            anchors.fill: boardView
            from: modelData.from != null ? toTileCenter(modelData.from) : null           
            to: modelData.to != null ? toTileCenter(modelData.to) : null
            tileWidth: boardView.tileWidth
            color: turnPlayer(forwardTurn + index)
            num: index + 1
        }
    }

    MouseArea {
        anchors.fill: parent
        enabled: true
        onClicked: {
            currentMovePossible = [];
        }
    }
}