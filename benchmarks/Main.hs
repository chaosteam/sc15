{-# LANGUAGE RecordWildCards #-}
module Main where

import Control.Applicative
import Control.Lens
import Control.Monad
import Criterion.Main
import Data.Maybe
import Data.Monoid
import Prelude

import Board
import BoardInfo
import Game
import PenguinState
import Protocol
import SC.GameDefinition
import SC.Replay
import SC.Strategy
import SC.Types
import Scoring
import Strategy

benchBoard :: (BoardFloes, GameState) -> [Benchmark]
benchBoard (_, GameState{..}) =
  [ bench "connectedComponents" $ nf connectedComponents _board
  , bench "stepReachableFold/dup" $ nf
      (stepReachableFold False (\_ -> flip insertFloe) _board firstFree) mempty
  , bench "stepReachableFold/no-dup" $ nf
      (stepReachableFold True (\_ -> flip insertFloe) _board firstFree) mempty
  , bench "reachableFrom" $ nf (reachableFrom firstFree) _board
  ]
 where
  firstFree = head $ freePositions _board

benchGame :: (BoardFloes, GameState) -> [Benchmark]
benchGame (floes, gs@GameState{..}) =
  [ bench "collectInfo" $ nf (collectInfo floes _board (ps^.self gc)) (ps^.enemy gc)
  , bench "computeScore" $ nf (computeScore gc floes) gs
  , bench "possibleNextStates/full" $ nf (possibleNextStates floes) gs
  , bench "possibleNextStates/count-only" $ nf (length . possibleNextStates floes) gs
  , bench "possibleNextStates/move-only" $ nf (map fst . possibleNextStates floes) gs
  , bench "possibleNextStates/state-only" $ nf (map snd . possibleNextStates floes) gs
  , bench "possibleMoves" $ nf (length . possibleMoves floes) gs
  ]
 where
  gc = GameConfiguration _nextPlayerColor PlayerRed
  ps = penguinPositions <$> _penguins


benchStrategy :: (BoardFloes, GameState) -> [Benchmark]
benchStrategy initial =
  [ bench "move1" $ nf (view _2 . head . moves . strategy gc GameUpdate) initial
  , bench "move2" $ nf (move 1 . strategy gc GameUpdate) initial
  , bench "move3" $ nf (move 2 . strategy gc GameUpdate) initial
  , bench "move4" $ nf (move 3 . strategy gc GameUpdate) initial
--  , bench "move5" $ nf (move 4 . strategy gc GameUpdate) initial
  ]
 where gc = GameConfiguration
         { selfColor = snd initial^.nextPlayerColor
         , startColor = PlayerRed
         }
       move n = fmap (view _2) . listToMaybe . drop n . moves

main :: IO ()
main = do
  let replayFiles = ["123662", "123674"]
      replayTurns = [0, 1, 4, 10, 11, 30, 34, 37, 50, 58, 59]
  replays <- forM replayFiles $ \file ->
    (,) file <$> loadReplayFromRaw heyThatsMyFish ("replays/" ++ file ++ ".xml")
  let benchAll f =
        [ bgroup replayName [bgroup (show i) (f (initials !! i)) | i <- replayTurns]
        | (replayName, Replay{..}) <- replays
        , let initials =
                map runIdentity $ (initState initialSituation : map initState turns)
        ]
  defaultMain
    [ bgroup "Board" $ benchAll benchBoard
    , bgroup "Strategy" $ benchAll benchStrategy
    , bgroup "Game" $ benchAll benchGame
    ]
