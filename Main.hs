module Main where

import SC.Main

import Protocol
import Strategy

main :: IO ()
main = defaultMain heyThatsMyFish
  [ ("default", Strategy.strategy)
  , ("null", \_ _ _ -> nullStrategy)
  ]
