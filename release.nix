{ haskellPackages ? import ./nix/lucid-haskell-packages.nix }:
let

  pkgs = import <nixpkgs> { config = {}; };
  nglib = pkgs.haskell-ng.lib;
  sc15expr = pkgs.stdenv.mkDerivation ({
    name = "sc15.nix";
    buildCommand = ''
      cp -r ${./.} $TMP/pkg-source
      chmod +rw -R $TMP/pkg-source
      sed -i -e 's/benchmark/test-suite/' "$TMP/pkg-source/"*.cabal
      ${pkgs.haskellngPackages.cabal2nix}/bin/cabal2nix $TMP/pkg-source > $out
    '';
  } // pkgs.lib.optionalAttrs pkgs.stdenv.isLinux {
    LANG = "en_US.UTF-8";
    LOCALE_ARCHIVE = "${pkgs.glibcLocales}/lib/locale/locale-archive";
  });

  myHaskellPackages = haskellPackages.override (old: {
    overrides = self: super: (old.overrides or (_: _: {})) self super // {
      sc15 = self.callPackage (import sc15expr) {};
      sclib = self.callPackage ./nix/sclib.nix {};
    };
  });

  sc15 = nglib.overrideCabal myHaskellPackages.sc15 (old: {
    enableSharedExecutables = false;
    doCheck = true;
    src = ./.;
    configureFlags = [ "--ghc-option=-Wall" "--ghc-option=-Werror" "--ghc-option=-O2" "--enable-benchmarks" ];
  });

  client = pkgs.stdenv.mkDerivation {
    name = "client";
    buildCommand = ''
      mkdir -p $out
      cp -a ${sc15}/bin/client $out/client
    '';
    allowedReferences = [
      haskellPackages.ekg # Executable references ekg data files
    ];
  };

  archive = with pkgs; stdenv.mkDerivation {
    name = "client-archive.zip";
    buildCommand = '' cd ${client} && ${zip}/bin/zip -r -o $out . '';
  };

in archive
