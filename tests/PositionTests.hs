{-# LANGUAGE TemplateHaskell #-}
module PositionTests (tests) where

import Control.Applicative
import Control.Lens hiding (elements)
import Data.Maybe
import Prelude
import Test.Tasty
import Test.Tasty.HUnit
import Test.Tasty.QuickCheck
import Test.Tasty.TH

import ArbitraryInstances()
import Position

case_all_positions_on_board :: Assertion
case_all_positions_on_board = all onBoard allPositions @?= True

prop_next_position_invertible :: Direction -> Position -> Bool
prop_next_position_invertible d p = fromMaybe True $
  maybe False (p ==) . nextPosition (oppositeDirection d) <$> nextPosition d p

prop_coordinates_iso_to_from :: Position -> Bool
prop_coordinates_iso_to_from = (==) <*> review coordinates . view coordinates

prop_coordinates_iso_from_to :: Property
prop_coordinates_iso_from_to = property $ do
  x <- elements [0..7]
  y <- elements [0..7]
  return $ (==) <*> view coordinates . review coordinates $ (x,y)

tests :: TestTree
tests = $testGroupGenerator
