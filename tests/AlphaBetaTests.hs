{-# LANGUAGE TupleSections #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE ScopedTypeVariables #-}
module AlphaBetaTests (tests) where

import AlphaBeta
import Board
import Game
import Score
import Scoring

import Control.Applicative
import Control.Arrow
import Data.List (maximumBy)
import Data.Monoid
import Data.Ord
import Prelude
import SC.Types

import ArbitraryInstances()
import Test.Tasty
import Test.Tasty.HUnit
import Test.Tasty.QuickCheck
import Test.Tasty.TH

data Tree m = Branch [(m, Tree m)] | Final Points

-- | Implement minimax for comparision to alpha beta
minimax :: Tree m -> (Points, [m])
minimax = go False where
  go :: Bool -> Tree m -> (Points, [m])
  go !isEnemy (Final s) = (adjust isEnemy s, [])
  go !isEnemy (Branch choices)
    = snd . maximumBy (comparing (fst . snd) <> flip (comparing fst)) $ zip [(0::Int)..] childs
   where
    childs = map (\(m,s) -> negate *** (m:) $ go (not isEnemy) s) choices

  adjust isEnemy = if isEnemy then negate else id

alphaBetaTree :: Tree m -> SearchM p () (PointsOf p, [m])
alphaBetaTree (Final s) = fmap (,[]) $ leafStatistic s *> fromFirstPoints s
alphaBetaTree (Branch cs) = alphaBeta . flip map cs $ \(m, tree') ->
  fmap (m :) <$> alphaBetaTree tree'

exampleTree1 :: Tree Int
exampleTree1 = branch
  [ branch [final [4], final [6,2,6], final [3, 9]]
  , branch [final [5,2], final [7,3]]
  , branch [final [1], final [7,2], final [4,6,3]]
  ]
 where
  branch = Branch . zip [0..]
  final = branch . map Final

case_alpha_beta_example_1 :: Assertion
case_alpha_beta_example_1
  = evalSearchFull (alphaBetaTree exampleTree1) () @?= (5, [1,0,0])

case_minimax_example_1 :: Assertion
case_minimax_example_1 = minimax exampleTree1 @?= (5, [1,0,0])

buildGameTree :: GameConfiguration -> BoardFloes -> GameState -> Int -> Tree Move
buildGameTree gc floes gs 0 = Final (computeScore gc floes gs)
buildGameTree gc floes gs n = Branch . flip map (take 20 $ nullMove ++ possible) $ \m ->
  (m, buildGameTree gc floes (applyMove floes gs m) (n - 1))
 where
  possible :: [Move]
  possible = possibleMoves floes gs

  nullMove :: [Move]
  nullMove = [ Null | not $ setPhase gs ]

prop_alpha_beta_same_score_minimax :: GameConfiguration -> BoardFloes -> GameState -> Bool
prop_alpha_beta_same_score_minimax gc floes gs =
  fst (minimax tree) == toFirstPoints (fst (evalSearchFull (alphaBetaTree tree) ()))
 where tree = buildGameTree gc floes gs 3


prop_alpha_beta_same_as_minimax :: GameConfiguration -> BoardFloes -> GameState -> Bool
prop_alpha_beta_same_as_minimax gc floes gs =
  minimax tree == first toFirstPoints (evalSearchFull (alphaBetaTree tree) ())
 where tree = buildGameTree gc floes gs 3

tests :: TestTree
tests = $testGroupGenerator
