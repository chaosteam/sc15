{-# LANGUAGE TemplateHaskell #-}
module StorableTests
  ( tests
  ) where

import Foreign.Marshal.Alloc
import Foreign.Storable

import Test.Tasty
import Test.Tasty.QuickCheck
import Test.Tasty.TH

import ArbitraryInstances()
import Game
import Position

testStorable :: (Eq a, Storable a) => a -> Property
testStorable x = ioProperty $ fmap (== x) $ alloca $ \ptr -> poke ptr x >> peek ptr

prop_storable_position :: Position -> Property
prop_storable_position = testStorable

prop_storable_move :: Move -> Property
prop_storable_move = testStorable

tests :: TestTree
tests = $testGroupGenerator
