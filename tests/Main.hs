module Main where

import Test.Tasty

import qualified AlphaBetaTests
import qualified BoardTests
import qualified EnumTests
import qualified PositionTests
import qualified StorableTests

main :: IO ()
main = defaultMain $ testGroup "softwarechallenge tests"
     [ AlphaBetaTests.tests
     , BoardTests.tests
     , EnumTests.tests
     , PositionTests.tests
     , StorableTests.tests
     ]
