{-# LANGUAGE TemplateHaskell #-}
module BoardTests where

import Control.Applicative
import Data.List (nub, sort)
import Data.Maybe
import Test.Tasty
import Test.Tasty.HUnit
import Test.Tasty.QuickCheck
import Test.Tasty.TH

import ArbitraryInstances()
import Board
import Position

prop_remove_find :: BoardFloes -> Position -> PositionSet -> Bool
prop_remove_find floes p b = findFloe p floes (removeFloe p b) == Nothing

prop_remove_preserve_other :: Position -> PositionSet -> Bool
prop_remove_preserve_other p b = all preserved $ filter (/= p) allPositions where
  b' = removeFloe p b
  preserved p' = freeFloe p' b' == freeFloe p' b

case_create_all_free :: Assertion
case_create_all_free =
  filter (`freeFloe` newPositionSet allPositions) [0..63] @?= allPositions

prop_free_find :: BoardFloes -> Position -> PositionSet -> Bool
prop_free_find floes p b = isJust (findFloe p floes b) == freeFloe p b

prop_free_floe_free_positions :: PositionSet -> Bool
prop_free_floe_free_positions b =
  sort [p | p <- allPositions, freeFloe p b] == sort (freePositions b)

prop_next_positions_free :: Position -> PositionSet -> Bool
prop_next_positions_free p b
  = all (`freeFloe` b) $ freePositions $ nextPositions b p

prop_connected_components_uniq_positions :: PositionSet -> Bool
prop_connected_components_uniq_positions = isUnique . concat . conn where
  isUnique a = nub a == a
  conn = map freePositions . connectedComponents

prop_connected_components_fill_board :: PositionSet -> Bool
prop_connected_components_fill_board = fillBoard <*> concat . conn where
  fillBoard b ps = sort ps == sort (freePositions b)
  conn = map freePositions . connectedComponents

case_connected_components :: Assertion
case_connected_components = do
  test [3, 10, 26, 36] [[3], [10], [26], [36]]
  test [0,1,2,10,19,27,42,43,55,63,61] [[0,1,2,10,19,27], [42,43], [55, 63], [61]]
 where
  test a b = deepSort (conn $ newPositionSet a) @?= deepSort b
  deepSort = sort . map sort
  conn = map freePositions . connectedComponents

tests :: TestTree
tests = $testGroupGenerator
