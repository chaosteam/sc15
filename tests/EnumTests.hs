{-# LANGUAGE TemplateHaskell #-}
module EnumTests ( tests ) where

import Test.Tasty
import Test.Tasty.QuickCheck
import Test.Tasty.TH

import ArbitraryInstances()
import Board
import Position

testEnum :: (Eq a, Enum a, Show a) => a -> Property
testEnum x = x === toEnum (fromEnum x)

prop_enum_position :: Position -> Property
prop_enum_position = testEnum

prop_enum_floe :: Floe -> Property
prop_enum_floe = testEnum

tests :: TestTree
tests = $testGroupGenerator
