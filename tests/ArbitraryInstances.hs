{-# OPTIONS_GHC -fno-warn-orphans #-}
{-# LANGUAGE BangPatterns #-}
module ArbitraryInstances where

import Control.Applicative
import Data.List (foldl', elemIndices)
import Prelude
import SC.Types
import Test.QuickCheck

import qualified Data.Vector.Unboxed as V

import Board
import Game
import PenguinState
import Position
import Score

instance Arbitrary Position where
  arbitrary = elements allPositions

instance Arbitrary Move where
  arbitrary = oneof [ pure Null, Set <$> arbitrary, Run <$> arbitrary <*> arbitrary ]

instance Arbitrary Floe where
  arbitrary = elements [ Sunken, OneFish, TwoFishes, ThreeFishes ]

instance Arbitrary Slope where
  arbitrary = elements [ Up, Same, Down ]

uniqueElements :: Eq a => Int -> [a] -> Gen [a]
uniqueElements 0 _ = pure []
uniqueElements !n xs = do
  x <- elements xs
  (x:) <$> uniqueElements (n - 1) (filter (/= x) xs)

instance Arbitrary PositionSet where
  arbitrary = do
    l <- elements [0..60]
    newPositionSet <$> uniqueElements l allPositions

instance Arbitrary BoardFloes where
  arbitrary = newBoardFloes . V.fromList <$> vectorOf 64 realFloe `suchThat` valid where
    realFloe = elements [OneFish, TwoFishes, ThreeFishes]
    valid = (>= 8) . length . elemIndices OneFish

instance Arbitrary PlayerColor where
  arbitrary = elements [PlayerRed, PlayerBlue]

instance Arbitrary GameState where
  arbitrary = do
    scoreA    <- Points <$> elements [0..100]
    scoreB    <- Points <$> elements [0..100]
    t <- elements [0..60]
    let next = if odd t then PlayerBlue else PlayerRed
        countA = min 4 $ (t + 1) `quot` 2
        countB = min 4 $ t `quot` 2        
    b <- if t <= 7
        then return $ newPositionSet allPositions
        else arbitrary `suchThat` ((>= 8) . length . freePositions)
    penguinsA <- uniqueElements countA (freePositions b)
    penguinsB <- uniqueElements countB . freePositions $ foldl' (flip removeFloe) b penguinsA

    return $ GameState
      (PlayerData (newPenguinState penguinsA) (newPenguinState penguinsB))
      (PlayerData scoreA scoreB)
      (foldl' (flip removeFloe) b (penguinsA ++ penguinsB))
      next
      t

instance Arbitrary GameConfiguration where
  arbitrary = GameConfiguration <$> arbitrary <*> arbitrary
