{-# LANGUAGE RecordWildCards #-}
{-# OPTIONS_GHC -funbox-strict-fields #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
module Game where

import Control.DeepSeq.Generics
import Control.Lens
import Data.Hashable
import Data.Serialize
import Data.Typeable
import Data.Word
import Foreign.Ptr
import Foreign.Storable
import GHC.Generics
import SC.Types

import qualified Data.Foldable as F

import Board
import PenguinState
import Position
import Score
--------------------------------------------------------------------------------

-- | A 'Move' is a description of a possible actions with the penguins.
data Move = Set Position | Run Position Position | Null
  deriving (Generic, Show, Eq, Typeable)
instance NFData Move where rnf = genericRnf
instance Hashable Move

instance Storable Move where
  sizeOf _ = sizeOf (undefined :: Position) * 2 + 1
  alignment _ = alignment (undefined :: Position)
  poke ptr (Set p) = do
    poke (castPtr ptr) p
    poke (ptr `plusPtr` (sizeOf p * 2)) (0 :: Word8)
  poke ptr (Run a b) = do
    poke (castPtr ptr) a
    poke (ptr `plusPtr` sizeOf a) b
    poke (ptr `plusPtr` (sizeOf a * 2)) (1 :: Word8)
  poke ptr Null = poke (ptr `plusPtr` (2 * sizeOf (undefined :: Position))) (2 :: Word8)
  peek ptr = do
    p1 <- peek (castPtr ptr)
    i :: Word8 <- peek (castPtr ptr `plusPtr` (sizeOf p1 * 2))
    case i of
      0 -> return $ Set p1
      2 -> return Null
      _ -> do
        p2 <- peek (castPtr ptr `plusPtr` sizeOf p1)
        return $ Run p1 p2

--------------------------------------------------------------------------------

-- | The 'GameUpdate' captures all possible updates during the game.
--
-- In the "Hey, That's my fish"  game,
-- all information required to fully simulate the game is already available at the start,
-- so this type has no fields.
data GameUpdate = GameUpdate

instance Storable GameUpdate where
  alignment _ = 1
  sizeOf _ = 1
  poke _ GameUpdate = return ()
  peek _ = return GameUpdate

--------------------------------------------------------------------------------

-- | The 'GameState' stores all informations about the state of the game.
data GameState = GameState
  { _penguins        :: !(PlayerData PenguinState)
  , _scores          :: !(PlayerData Points)
  , _board           :: !PositionSet
  , _nextPlayerColor :: !PlayerColor
  , _turn            :: !Int
  } deriving (Generic, Eq, Show)
makeLenses ''GameState
instance Serialize GameState
instance NFData GameState where rnf = genericRnf

instance Hashable GameState where
  hashWithSalt salt0 GameState{..} =
   let
    salt1 = F.foldl' hashWithSalt salt0 _penguins
    salt2 = F.foldl' hashWithSalt salt1 _scores
   in salt2 `hashWithSalt` _board

-- | Check if we are still in the set phase.
setPhase :: GameState -> Bool
setPhase gs = gs^.turn < 8

-- | Get the next 'GameState' by appling the given 'Move'.
applyMove :: BoardFloes -> GameState -> Move -> GameState
applyMove floes gs m = case m of
    Null    -> gs'
    Set p   -> gs'' p & penguins.player %~ setPenguin p
    Run s t -> gs'' t & penguins.player %~ movePenguin s t
  where
    gs' :: GameState
    gs' = gs & turn +~ 1 & nextPlayerColor %~ oppositePlayerColor

    gs'' :: Position -> GameState
    gs'' position = gs'
      & scores.player +~ fromIntegral (fromEnum $ floeAt position floes)
      & board         %~ removeFloe position

    player :: Lens' (PlayerData a) a
    player = playerWithColor $ gs^.nextPlayerColor

-- | Get all set 'Move's that are possible in the current situation.
possibleSetMoves :: BoardFloes -> GameState -> [Move]
possibleSetMoves floes gs
  = [ Set p | p <- freePositions (gs^.board), floeAt p floes == OneFish ]

-- | Get all run 'Move's that are possible in the current situation.
possibleRunMoves :: GameState -> [Move]
possibleRunMoves gs = concatMap findMoves $ penguinPositions penguinState
 where
  penguinState = gs^.penguins.playerWithColor (gs^.nextPlayerColor)
  findMoves start =
    [ Run start target | target <- freePositions $ nextPositions (gs^.board) start ]

-- | Get all 'Move's that are possible in the current situation,
-- except the 'Null :: Move'.
possibleMoves :: BoardFloes -> GameState -> [Move]
possibleMoves floes gs
  | setPhase gs  = possibleSetMoves floes gs
  -- Turn 8 is the turn directly after the set phase.
  -- Player blue may move two times in a row at this point.
  -- We implement this by making Player A always do a Null move here.
  | gs^.turn == 8 = [ Null ]
  | otherwise    = possibleRunMoves gs

-- | Compute the possible moves and return the move and the state of the game after
-- applying the move. If the game has finished (either because no player can move anymore
-- or the turn limit is exceeded), the returned list is empty.
possibleNextStates :: BoardFloes -> GameState -> [(Move, GameState)]
possibleNextStates floes gs
  | finished = []
  | otherwise = map (\m -> (m, applyMove floes gs m)) $ nullMove ++ possible
 where
  possible :: [Move]
  possible = possibleMoves floes gs

  nullMove :: [Move]
  nullMove = [ Null | not $ setPhase gs ]

  next :: [Move]
  next = possibleMoves floes $ applyMove floes gs Null

  finished :: Bool
  finished = gs^.turn > 60 || null possible && null next
