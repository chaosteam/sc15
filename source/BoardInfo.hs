{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DeriveGeneric #-}
-- | This module provides a common data type
-- to store information about the board and a game situation.
-- Having a data type means that
-- the information is only collected once and not recomputed every time.
module BoardInfo
  ( ComponentInfo(..)
  , PenguinInfo(..)
  , PenguinID(), ComponentID(), BoardInfo()
  , availableComponents
  , availablePenguins
  , getComponentInfo
  , getPenguinInfo
  , getComponentPenguins
  , getPenguinComponents
  , collectInfo
  , singleComponentInfo
  , assignPenguins
  ) where

import Control.Applicative
import Control.DeepSeq
import Control.DeepSeq.Generics
import Control.Monad
import Control.Monad.ST
import Data.Monoid
import Data.Ord
import Data.Vector (Vector)
import GHC.Generics
import Prelude
import Safe

import qualified Data.Vector as V
import qualified Data.Vector.Mutable as VM

import Board
import Position

-- | Information that might be useful about a component.
--
-- Stored in a datatype so that it is cached.
data ComponentInfo = ComponentInfo
  { componentPositions :: PositionSet
  , componentFishCount :: Int
  } deriving (Show, Eq, Ord, Generic)
instance NFData ComponentInfo where rnf = genericRnf

-- | Information that might be useful about a single penguin.
--
-- Stored in a so that it is cached.
data PenguinInfo = PenguinInfo
  { penguinPosition :: Position
  , penguinNeighbours :: PositionSet
  , penguinIsEnemy :: Bool
  } deriving (Show, Eq, Ord, Generic)
instance NFData PenguinInfo where rnf = genericRnf

-- | Identifier for a penguin.
newtype PenguinID = PenguinID Int deriving (Show, Eq, Ord, NFData)
-- | Identifier for a component.
newtype ComponentID = ComponentID Int deriving (Show, Eq, Ord, NFData)

-- | Information about the board used for evaluation.
data BoardInfo f = BoardInfo
  { penguinInfos :: Vector PenguinInfo
  , componentInfos :: Vector ComponentInfo
  , penguinsForComponent :: Vector [PenguinID]
  , componentsForPenguin :: Vector (f ComponentID)
  } deriving Generic
instance NFData (f ComponentID) => NFData (BoardInfo f)

-- | Get information about a given component.
getComponentInfo :: ComponentID -> BoardInfo f -> ComponentInfo
getComponentInfo (ComponentID x) = flip V.unsafeIndex x . componentInfos

-- | Get information about a particular penguin.
getPenguinInfo :: PenguinID -> BoardInfo f -> PenguinInfo
getPenguinInfo (PenguinID x) = flip V.unsafeIndex x . penguinInfos

-- | Get the components assigned to a particular penguin.
getPenguinComponents :: PenguinID -> BoardInfo f -> f ComponentID
getPenguinComponents (PenguinID x) = flip V.unsafeIndex x . componentsForPenguin

-- | Get the penguins that are assigned to a particular component.
getComponentPenguins :: ComponentID -> BoardInfo f -> [PenguinID]
getComponentPenguins (ComponentID x) = flip V.unsafeIndex x . penguinsForComponent

-- | Return all available components.
availableComponents :: BoardInfo f -> [ComponentID]
availableComponents BoardInfo{..} = map ComponentID [0 .. (V.length componentInfos - 1)]

-- | Return all avaiable penguins.
availablePenguins :: BoardInfo f -> [PenguinID]
availablePenguins BoardInfo{..} = map PenguinID [0 .. (V.length penguinInfos - 1)]

-- | Collect information about a game situation that is useful for evaluating it.
collectInfo :: BoardFloes
            -> PositionSet -- ^ The positions of free floes of the board
            -> [Position] -- ^ Positions of our own penguins
            -> [Position] -- ^ Positions of enemy penguins
            -> BoardInfo []
collectInfo floes board positionsSelf positionsEnemy = BoardInfo{..} where
  penguinInfos = V.fromList $
    map (makePenguinInfo False) positionsSelf ++ map (makePenguinInfo True) positionsEnemy
  componentInfos = V.fromList . map (makeComponentInfo floes) $ connectedComponents board

  (penguinsForComponent, componentsForPenguin) = runST $ do
    nearComponent <- VM.replicate (V.length componentInfos) []
    nearPenguin   <- VM.replicate (V.length penguinInfos) []
    V.forM_ (V.indexed componentInfos) $ \ (componentIndex, component) ->
      V.forM_ (V.indexed penguinInfos) $ \ (penguinIndex,   penguin)   ->
        when (componentPositions component `overlapping` penguinNeighbours penguin) $ do
          modify nearComponent componentIndex (PenguinID   penguinIndex   :)
          modify nearPenguin   penguinIndex   (ComponentID componentIndex :) 
    (,) <$> V.freeze nearComponent <*> V.freeze nearPenguin

  x `overlapping` y = x `intersection` y /= mempty

makeComponentInfo :: BoardFloes -> PositionSet -> ComponentInfo
makeComponentInfo floes component = ComponentInfo component $ countFishes floes component

makePenguinInfo :: Bool -> Position -> PenguinInfo
makePenguinInfo isEnemy position = PenguinInfo position (neighbours position) isEnemy

-- | Assign each penguin, such that there no more than one component for each penguin.
assignPenguins :: BoardInfo [] -> BoardInfo Maybe
assignPenguins info@BoardInfo{..} = info
  { componentsForPenguin = assignments
  , penguinsForComponent = nearComponent
  }
 where
  assignments = V.map choose componentsForPenguin
  choose = maximumByMay . comparing $ componentFishCount . flip getComponentInfo info
  nearComponent = runST $ do
    vector <- VM.replicate (V.length penguinsForComponent) []
    V.forM_ (V.indexed assignments) $ \ (penguinIndex, component) -> case component of
      Nothing                           -> return ()
      Just (ComponentID componentIndex) ->
        modify vector componentIndex (PenguinID penguinIndex :)
    V.freeze vector

-- | Make a BoardInfo with only a single component. The arguments are the same as for
-- 'collectInfo'.
singleComponentInfo :: BoardFloes -> PositionSet
                    -> [Position] -> [Position]
                    -> BoardInfo Maybe
singleComponentInfo floes b positionsSelf positionsEnemy = BoardInfo{..} where
  penguinInfos = V.fromList
     $ map (makePenguinInfo False) positionsSelf
    ++ map (makePenguinInfo True) positionsEnemy
  componentInfos = V.singleton $ makeComponentInfo floes b
  penguinsForComponent = V.singleton $ map PenguinID [0..(V.length penguinInfos-1)]
  componentsForPenguin = Just (ComponentID 0) <$ penguinInfos
  

modify :: VM.STVector s a -> Int -> (a -> a) -> ST s ()
modify vector index f = VM.read vector index >>= VM.write vector index . f
