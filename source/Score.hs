{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE MultiParamTypeClasses #-}
module Score where

import Control.DeepSeq.Generics
import Control.Lens
import Data.Hashable
import Data.Semigroup
import Data.Serialize

newtype Points = Points Int
  deriving (Real, Integral, Num, Read, Eq, Ord, Enum, NFData, Serialize, Hashable)
makeWrapped ''Points

instance Show Points where show (Points p) = show p

-- | This bounded instance guarrantes that each Score has a negation.
-- (The default instance for Int fails this because @-(minBound :: Int) == minBound@).
instance Bounded Points where
  minBound = Points $ minBound + 1
  maxBound = Points maxBound

-- | Additive 'Semigroup' for 'Points'.
instance Semigroup Points where
  Points a <> Points b = Points $ a + b

-- | Additive 'Monoid' for 'Points'
instance Monoid Points where
  mempty = Points 0
  mappend = (<>)
