{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE RecordWildCards #-}
module Board
  ( Floe(..)
  , BoardFloes()
  , PositionSet
  , newBoardFloes
  , newPositionSet
  , findFloe
  , floeAt
  , freePositions
  , removeFloe
  , removeFloes
  , insertFloe
  , countPositions
  , countFishes
  , neighbours
  , intersection
  , nextPositions
  , freeFloe
  , stepReachableFold
  , connectedComponents
  , reachableFrom
  ) where

import Position

import Control.Applicative
import Control.DeepSeq
import Control.Monad
import Data.Bits
import Data.Bits.Extras
import Data.Hashable
import Data.List (foldl')
import Data.Monoid
import Data.Serialize
import Data.Vector.Serialize()
import Data.Vector.Unboxed (Vector)
import Data.Vector.Unboxed.Deriving
import Data.Word
import GHC.Generics
import Prelude

import qualified Data.Vector.Unboxed as V

-- | The 'Floe' either contains some fishes or is sunken.
data Floe = Sunken | OneFish | TwoFishes | ThreeFishes
  deriving (Show, Generic, Eq, Ord, Bounded)
instance Serialize Floe
instance Enum Floe where
  toEnum 1 = OneFish
  toEnum 2 = TwoFishes
  toEnum 3 = ThreeFishes
  toEnum _ = Sunken

  fromEnum Sunken      = 0
  fromEnum OneFish     = 1
  fromEnum TwoFishes   = 2
  fromEnum ThreeFishes = 3

derivingUnbox "Floe"
  [t| Floe -> Int |]
  [|  fromEnum   |]
  [|  toEnum     |]

-- | Store the floes assigned to each position on the board. 
newtype BoardFloes = BoardFloes { floesVector :: Vector Floe }
  deriving (Show, Generic, Eq, Serialize, NFData)

-- | Create a new 'BoardFloes' from a vector
-- mapping the enum value of a 'Position' to the floe at that position.
-- The vector should have exactly 64 elements.
newBoardFloes :: Vector Floe -> BoardFloes
newBoardFloes floes
  | V.length floes /= 64 = error "newBoardFloes: length of vector is not 64"
  | otherwise           = BoardFloes floes

-- | Return the floe at the given position.
floeAt :: Position -> BoardFloes -> Floe
floeAt pos = flip V.unsafeIndex (fromEnum pos) . floesVector

-- | A 'PositionSet' stores a set of free positions.
newtype PositionSet = PositionSet { freeFloesBitset :: Word64 }
  deriving (Show, Generic, Eq, Serialize, Hashable, NFData, Ord)

derivingUnbox "PositionSet"
  [t| PositionSet -> Word64 |]
  [|  freeFloesBitset      |]
  [|  PositionSet          |]

-- | The monoid takes the union of two position sets.
instance Monoid PositionSet where
  mempty = PositionSet zeroBits
  mappend (PositionSet a) (PositionSet b) = PositionSet $ a .|. b

-- | Find all free 'Position's on the 'Board'.
--
-- A 'Position' is free,
-- if no penguin is on the corresponding 'Floe' and this 'Floe' still contains fish.
freePositions :: PositionSet -> [Position]
freePositions = go . freeFloesBitset where
  go 0     = []
  go !free = toEnum x : go (free `clearBit` x) where
    x = fromIntegral $ trailingZeros free

-- | Create a 'PositionSet' from a list of free 'Position's.
newPositionSet :: [Position] -> PositionSet
newPositionSet positions = PositionSet $ bitset .&. complement invalidPositions
 where invalidPositions  = zeroBits `setBit` 56 `setBit` 58 `setBit` 60 `setBit` 62
       bitset = foldl' setBit zeroBits $ map fromEnum positions

-- | Remove a 'Position' from the 'PositionSet'.
-- Does nothing if the position was not already in the set.
removeFloe :: Position -> PositionSet -> PositionSet
removeFloe position (PositionSet free) = PositionSet $ free `clearBit` fromEnum position

-- | Remove multiple floes from another 'PositionSet'.
removeFloes :: PositionSet -> PositionSet -> PositionSet
removeFloes (PositionSet a) (PositionSet b) = PositionSet $ a .&. complement b

-- | Insert a new position into the 'PositionSet'.
insertFloe :: Position -> PositionSet -> PositionSet
insertFloe position (PositionSet free) = PositionSet $ free `setBit` fromEnum position

-- | Get all 'Position's a penguin at the given 'Position' could move to in one 'Move'.
--
-- The start position does not have to be on the board,
-- the neighbour positions are included even in that case.
nextPositions :: PositionSet -> Position -> PositionSet
nextPositions board startPosition =
  go (Left Up) . go (Left Down) . go (Left Same) .
  go (Right Up) . go (Right Down) . go (Right Same) $
  mempty
 where
  go :: Direction -> PositionSet -> PositionSet
  go direction !reachable = go' reachable $ nextPosition direction startPosition where
    go' :: PositionSet -> Maybe Position -> PositionSet
    go' !reachable' (Just position) | freeFloe position board =
      go' (insertFloe position reachable') $ nextPosition direction position
    go' !reachable' _ = reachable'
  {-# INLINE go #-}

-- | Test whether a 'Position' is part of this 'PositionSet'.
freeFloe :: Position -> PositionSet -> Bool
freeFloe position = (`testBit` fromEnum position) . freeFloesBitset

-- | Calculate the intersection of two 'PositionSet's.
intersection :: PositionSet -> PositionSet -> PositionSet
intersection (PositionSet a) (PositionSet b) = PositionSet $ a .&. b

-- | Count the number of free positions in this 'PositionSet'.
countPositions :: PositionSet -> Int
countPositions = fromIntegral . populationCount . freeFloesBitset

-- | Count the fishes in the given 'PositionSet'.
countFishes :: BoardFloes -> PositionSet -> Int
countFishes floes free = sum [ fromEnum $ floeAt p floes | p <- freePositions free ]

-- | Return the neighbours of the given 'Position'.
neighbours :: Position -> PositionSet
neighbours p = neighbourFold p (flip insertFloe) mempty

-- | Fold all 'Position's reachable by movements with a penguin
-- starting at the given 'Position',
-- by first folding the immediately reachable 'Position's
-- and then combining the results with the 'Position's reachable in 2 steps, and so on,
-- until all 'Position's have been reached at least once.
--
-- The folding function receives the minimal number of steps
-- necessary to reach the folded 'Position's as an argument.
--
-- The first argument controls the folding of 'Position's during one step.
-- If it is 'True'
-- then 'Position's that can be reached in two or more ways will only be folded once,
-- otherwise they will be folded as many times as there are ways to reach them.
-- 'Position's that can also be reached in less steps are never included though.
--
-- Just as for 'reachablePositions', the start position does not have to be on the board.
-- Neighbours will still be included in the search. The start position itself is not
-- included in the fold.
stepReachableFold :: forall a. Bool -> (Int -> a -> Position -> a) -> PositionSet -> Position -> a -> a
-- Note: We want this to inline as soon as it gets three arguments,
-- so we put in a lambda to make it already saturated with three arguments.
stepReachableFold removeDuplicates fold PositionSet{..} =
   \ p x -> go freeFloesBitset 1 x [p]
 where
  go :: Word64 -> Int -> a -> [Position] -> a
  go _    _      result [] = result
  go free !steps result ps = go free' (steps + 1) result' ps' where
    result' = foldl' (fold steps) result ps'
    free'   = foldl' (flip $ flip clearBit . fromEnum) free ps
    ps' = uniq 0
        $ concatMap (freePositions . nextPositions (PositionSet free')) ps
    -- Note: The next step only contains positions not seen in all previous steps.
    -- This ensures we always use the minmal number of steps for the fold.

  uniq :: Word64 -> [Position] -> [Position]
  uniq _     []                                 = []
  uniq _     xs     | not removeDuplicates        = xs
  uniq !seen (x:xs) | seen `testBit` fromEnum x = uniq seen xs
                    | otherwise                 = x : uniq (seen `setBit` fromEnum x) xs
{-# INLINE stepReachableFold #-}

-- | Find all positions that are reachable from the given position.
--
-- The starting position does not have to be contained in the position set.
reachableFrom :: Position -> PositionSet -> PositionSet
reachableFrom start b = b `removeFloes` neighbourFold start go b where
  go :: PositionSet -> Position -> PositionSet
  go free position
    | freeFloe position free = neighbourFold position go $ removeFloe position free
    | otherwise = free

-- | Find all connected components of the 'Board'.
--
-- A connected component is the set of all 'Floe's that are reachable from each other.
-- No 'Floe' from one connected component is reachable from any other connected component.
connectedComponents :: PositionSet -> [PositionSet]
connectedComponents = go . freeFloesBitset where
  go :: Word64 -> [PositionSet]
  go 0    = []
  go free = reachable : go (freeFloesBitset free') where
    startPosition = toEnum . fromIntegral $ trailingZeros free
    reachable = insertFloe startPosition . reachableFrom startPosition $ PositionSet free
    free' = PositionSet free `removeFloes` reachable

-- | Check if the given position is free, and if so, return the 'Floe' at that position.
-- Otherwise return 'Nothing'.
findFloe :: Position -> BoardFloes -> PositionSet -> Maybe Floe
findFloe position floes free = floeAt position floes <$ guard (freeFloe position free)
