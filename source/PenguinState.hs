{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
module PenguinState
  ( PenguinState()
  , newPenguinState
  , penguinPositions
  , setPenguin
  , movePenguin
  , hasPenguin
  , allPenguinsSet
  ) where

import Control.Applicative
import Control.DeepSeq
import Control.Lens
import Data.Hashable
import Data.List (foldl')
import Data.Serialize
import Prelude

import Position

-- | The 'PenguinState' stores the 'Position's of the penguins of one player.
newtype PenguinState = PenguinState [Position]
  deriving (Eq, Show)
makeWrapped ''PenguinState
instance NFData PenguinState where
  rnf (PenguinState s) = rnf s

instance Serialize PenguinState where
  get = PenguinState <$> get
  put = put . view _Wrapped

instance Hashable PenguinState where
  hashWithSalt salt (PenguinState s) = foldl' hashWithSalt salt s

-- | Create a new 'PenguinState' from the given penguin 'Position's.
newPenguinState :: [Position] -> PenguinState
newPenguinState = PenguinState

-- | Get the 'Position's of all penguins.
penguinPositions :: PenguinState -> [Position]
penguinPositions = view _Wrapped

-- | Store the 'Position' of a newly set penguin.
setPenguin :: Position -> PenguinState -> PenguinState
setPenguin = over _Wrapped . (:)

-- | Change the 'Position' of a moving penguin.
movePenguin :: Position -> Position -> PenguinState -> PenguinState
movePenguin start target
  = over _Wrapped $ (target:) . filter (/= start)

-- | Check if one of the penguins is at this 'Position'.
hasPenguin :: Position -> PenguinState -> Bool
hasPenguin position = views _Wrapped $ elem position

-- | Check if all penguins are set.
allPenguinsSet :: PenguinState -> Bool
allPenguinsSet = views _Wrapped $ (== 4) . length
