{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Position where

import Control.DeepSeq
import Control.Lens
import Data.Bits
import Data.Hashable
import Data.Ix
import Data.Serialize
import Data.Typeable
import Data.Word
import Foreign.Storable (Storable(..))

-- | A 'Position' is an index into the 'Board'.
--
-- The 'Board' consists of 60 fields, which are indexed as follows:
--
--  00 08 16 24 32 40 48 _
-- 01 09 17 25 33 41 49 57
--  02 10 18 26 34 42 50 _
-- 03 11 19 27 35 43 51 59
--  04 12 20 28 36 44 52 _
-- 05 13 21 29 37 45 53 61
--  06 14 22 30 38 46 54 _
-- 07 15 23 31 39 47 55 63
--
-- The spots marked with _ would have the indices 56, 58, 60 and 62 respectively,
-- but those do not belong to the 'Board'.
newtype Position = Position Word8
  deriving (Show, Eq, Ord, Enum, Storable, NFData, Num, Ix, Bounded, Typeable, Hashable, Serialize)
makeWrapped ''Position

-- | The list of all valid 'Position's.
allPositions :: [Position]
allPositions = [Position x | x <- [0 .. 63], onBoard $ Position x]

-- | Convert between @(column, row)@ coordinates and 'Position's.
--
-- 'Position's 00 to 07 belong to the first column,
-- 08 to 15 to the second column and so on.
--
-- Column and row indices begin at zero.
--
-- >>> Position 17 ^. coordinates
-- (2,1)
--
-- >>> coordinates # (3,4)
-- Position 28
coordinates :: Iso' Position (Word8, Word8)
coordinates = _Wrapped . iso toCoordinates fromCoordinates where
  toCoordinates p = (p `shiftR` 3, p .&. 0x7)
  fromCoordinates (x,y) = x `shiftL` 3 .|. y

-- | The 'Slope' specifies one of the three kinds of "floe lines".
data Slope = Up | Same | Down deriving (Show, Eq)
instance NFData Slope where rnf a = seq a ()

-- | Get the corresponding 'Slope' for backwards movement.
oppositeSlope :: Slope -> Slope
oppositeSlope Up   = Down
oppositeSlope Down = Up
oppositeSlope Same = Same

-- | The 'Direction' is used to specify over which edge of a 'Floe' to move to the next.
type Direction = Either Slope Slope

-- | The list of all 'Directions'.
allDirections :: [Direction]
allDirections = [d s | d <- [Left, Right], s <- [Up, Same, Down]]

-- | Get corresponding 'Direction' for backwards movement.
oppositeDirection :: Direction -> Direction
oppositeDirection = either Right Left . over both oppositeSlope

-- | Go one step in the given 'Direction'.
--
-- If that would result in a 'Position' not on the board, 'Nothing' is returned.
nextPosition :: Direction -> Position -> Maybe Position
nextPosition direction (Position x) = go direction
  where
    go :: Direction -> Maybe Position
    go (Left Up)    | odd' x  && notLeft              = Just . Position $ x - 9
                    | even' x &&            notTop    = Just . Position $ x - 1
    go (Left Same)  |           notLeft              = Just . Position $ x - 8
    go (Left Down)  | odd' x  && notLeft  && notBottom = Just . Position $ x - 7
                    | even' x                        = Just . Position $ x + 1
    go (Right Up)   | odd' x  && notRight             = Just . Position $ x - 1
                    | even' x &&            notTop    = Just . Position $ x + 7
    go (Right Same) |           notRight'            = Just . Position $ x + 8
    go (Right Down) | odd' x  && notRight && notBottom = Just . Position $ x + 1
                    | even' x                        = Just . Position $ x + 9
    go _                                             = Nothing

    notTop    = x .&. 7 /= 0
    notBottom = x .&. 7 /= 7
    notLeft   = x > 7
    notRight  = x < 56
    notRight' = x < 48 || (odd' x && x < 56)

    odd'  = (`testBit` 0)
    even' = not . odd'
{-# INLINE nextPosition #-}


-- | Fold over the neighbours of the given position
neighbourFold :: Position -> (a -> Position -> a) -> a -> a
neighbourFold (Position x) f a0
  | odd' x    = goOdd
  | otherwise = goEven
  where
    goOdd = a4 where
      a1 | notLeft = a0 `f` Position (x - 9) `f` Position (x - 8)   | otherwise = a0
      a2 | notLeft && notBottom = a1 `f` Position (x - 7)            | otherwise = a1
      a3 | notRight = a2 `f` Position (x - 1) `f` Position (x + 8)  | otherwise = a2
      a4 | notRight && notBottom = a3 `f` Position (x + 1)           | otherwise = a3

    goEven = a4 where
      a1 = a0 `f` Position (x + 1) `f` Position (x + 9)          
      a2 | notTop = a1 `f` Position (x - 1) `f` Position (x + 7) | otherwise = a1
      a3 | notLeft = a2 `f` Position (x - 8)                     | otherwise = a2
      a4 | notRight' = a3 `f` Position (x + 8)                   | otherwise = a3
    
    notTop    = x .&. 7 /= 0
    notBottom = x .&. 7 /= 7
    notLeft   = x > 7
    notRight  = x < 56
    notRight' = x < 48

    odd'  = (`testBit` 0)
{-# INLINE neighbourFold #-}

-- | Check if the 'Position' is a valid index into the 'Board'.
onBoard :: Position -> Bool
onBoard (Position x) = x /= 56 && x /= 58 && x /= 60 && x /= 62
