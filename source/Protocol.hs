{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE LambdaCase #-}
module Protocol (heyThatsMyFish) where

import Control.Applicative
import Control.Error hiding ((??))
import Control.Guard
import Control.Lens hiding (element)
import Data.List (findIndices)
import Prelude
import SC.Error
import SC.GameDefinition
import SC.LaxXML
import SC.Protocol
import SC.Types

import qualified Data.Text as T
import qualified Data.Vector.Unboxed as V

import Board
import Game
import Score
import PenguinState
import Position

-- | Game definition for the "Hey, That's my Fish" game.
heyThatsMyFish :: GameDefinition (BoardFloes, GameState) Move GameUpdate
heyThatsMyFish = GameDefinition
  { gameType = T.pack "swc_2015_hey_danke_fuer_den_fisch"
  , mementoParser = parser
  , sendMove = send
  }

-- | Send a 'Move'.
send :: Monad m => Move -> Producer m Event
send (Set position) = sendTag "data"
  [ ("class", "SetMove")
  , ("setX" , T.pack $ show x)
  , ("setY" , T.pack $ show y)
  ] where (x,y) = position^.coordinates
send (Run start end) = sendTag "data"
  [ ("class", "RunMove")
  , ("fromX", T.pack $ show startx)
  , ("fromY", T.pack $ show starty)
  , ("toX"  , T.pack $ show endx)
  , ("toY"  , T.pack $ show endy)
  ] where
    (startx, starty) = start^.coordinates
    (endx,   endy)   =   end^.coordinates
send Null = sendTag "data" [("class", "NullMove")]

-- | Parse a 'Move' from the given attributes.
moveAttributes :: AttributeParser (Maybe Move)
moveAttributes = textAttribute "class" >>= \case
    "SetMove" -> Just <$> setMove
    "RunMove" -> Just <$> runMove
    "NullMove" -> return $ Just Null
    _          -> return Nothing
 where
  setMove = Set <$> (toPosition <$> readAttribute "setX"  <*> readAttribute "setY" )
  runMove = Run <$> (toPosition <$> readAttribute "fromX" <*> readAttribute "fromY")
                <*> (toPosition <$> readAttribute "toX"   <*> readAttribute "toY"  )
  toPosition = curry (review coordinates)

-- | Parse the color of the owner of a penguin.
parsePenguin :: Parser' PlayerColor
parsePenguin = tag "penguin" $ attribute "owner" readPlayerColor

-- | Parse a field.
--
-- Return the 'Floe'
-- and maybe the color of the player owning a penguin on it.
parseField :: Parser' (Floe, Maybe PlayerColor)
parseField = element "field" (readAttribute "fish")
           $ \ count -> (toEnum count, ) <$> maybeEOF parsePenguin

-- | Parse an array of fields, that is, a "column" of 'Floe's.
parseFieldArray :: Parser' [(Floe, Maybe PlayerColor)]
parseFieldArray = parentName "field-array" $ untilEOF parseField

-- | Get the 'Position' of the first field on which a penguin could be set.
--
-- This parses fields until it finds such a "free" field.
parseUntilFreeField :: Parser' Position
parseUntilFreeField = go $ Position 0 where
  go :: Position -> Parser' Position
  go position = do
    (fishCount, penguin) <- parseField
    if fishCount == OneFish && penguin == Nothing
      then pure position
      else go $ position & _Wrapped +~ 1

-- | Create the 'BoardFloes', 'PositionSet' and 'PenguinState's from a list of fields.
createBoard :: [(Floe, Maybe PlayerColor)]
            -> (BoardFloes, PositionSet, PlayerData PenguinState)
createBoard fields =
    (newBoardFloes (V.fromList fishCounts), newPositionSet notSunken, penguinStates)
  where
    (fishCounts, penguinOwners) = unzip fields
    positions = zip penguinOwners $ map Position [0..]
    notSunken = map toEnum $ findIndices (/= Sunken) fishCounts
    playerPositions player =
      [ x | (penguinOwner, x) <- positions, penguinOwner == Just player ]
    penguinStates = PlayerData
      (newPenguinState $ playerPositions PlayerRed)
      (newPenguinState $ playerPositions PlayerBlue)

-- | Parse the contents of the fields element of a board element.
parseBoard :: Monad m => Parser o m a -> Parser o m a
parseBoard = parentName "board" . parentName "fields"

-- | Remove the penguin positions from the game state
-- and add their FishCount to the points.
removePenguins :: BoardFloes -> GameState -> GameState
removePenguins floes = go PlayerRed . go PlayerBlue where
  go color gs = gs & scores.playerWithColor color +~ points
                   & board %~ (`removeFloes` positionSet)
   where
    positions   = penguinPositions $ gs^.penguins.playerWithColor color
    positionSet = newPositionSet positions
    points      = fromIntegral $ sum [ fromEnum $ floeAt p floes | p <- positions ]

-- | Get a parser to parse information from the server about the game.
parser :: PlayerColor -> MementoParser (BoardFloes, GameState) Move GameUpdate
parser _ = MementoParser ((,) <$> textAttribute "currentMoveType" <*> readAttribute "turn") $
  \ (moveType, turnNumber) currentColor -> do
    guardedPoints <- guarded $ parsePlayerData (Points <$> readAttribute "points")
    fallbackMove <- if moveType == "RUN"
      then return Null
      else lookahead . parseBoard $ Set <$> parseUntilFreeField
    state <- guarded $ parseBoard $ do
      fields <- untilEOF parseFieldArray
      let (floes, currentBoard, penguinStates) = createBoard (concat fields)
          turnNumber' | turnNumber > 7 = turnNumber + 1 | otherwise = turnNumber
      pure $ \ points -> ( floes, removePenguins floes $
        GameState penguinStates points currentBoard currentColor turnNumber')
    previousMove <- guarded $ element "lastMove" moveAttributes $
      flip maybe return $ throwE $ AttributeParseFailed "move" "No such move type"
    return $ TurnState (state <*> guardedPoints) fallbackMove previousMove GameUpdate
