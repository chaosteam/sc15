{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE BangPatterns #-}
module Strategy where

import Control.Applicative
import Control.Lens
import Control.Monad
import Data.List (sortBy)
import Data.Maybe
import Data.Ord
import Prelude
import SC.Strategy
import SC.Types

import qualified Data.Foldable as F
import qualified Data.HashMap.Strict as HM
import qualified Data.Traversable as T

import AlphaBeta
import Board
import Game
import Score
import Scoring

endSearch :: GameConfiguration -> BoardFloes -> GameState -> SearchM p s (PointsOf p, [Move])
endSearch gc floes gs = fmap (, []) $ leafStatistic score >> fromFirstPoints score
  where !score = computeScore gc floes gs

data SearchState = SearchState
  { _transpositionTable :: !(HM.HashMap PositionSet Points)
  , _refutationTable :: !(HM.HashMap Move [(Points, Move)])
  } 
makeLenses ''SearchState

saved :: GameState
       -> SearchM p SearchState (PointsOf p, a)
       -> SearchM p SearchState (PointsOf p, a)
saved gs search = do
  r <- search
  transpositionTable %= HM.insert (gs^.board) (toRelativePoints $ fst r)
  return r

searchDepth :: GameConfiguration -> BoardFloes -> GameState -> Int -> Maybe Move
            -> SearchM p SearchState (PointsOf p, [Move])
searchDepth gc floes gs 0 _ = saved gs $ endSearch gc floes gs
searchDepth gc floes gs d previous
  | null states = saved gs $ endSearch gc floes gs
  | otherwise = do
    table <- use transpositionTable
    saved gs $ do
      best <- fmap (fromMaybe [] . join) $ T.forM previous $ \p ->
        uses refutationTable $ HM.lookup p
      r <- alphaBeta . order $ states <&> \(m, gs') ->
        let
          prevscore
            | m `elem` map snd best = minBound
            | otherwise = fromMaybe maxBound $ HM.lookup (gs'^.board) table
        in (prevscore, insertMove m <$> searchDepth gc floes gs' (d - 1) (Just m))
      when (gs^.turn /= 8) $ forOf_ _head (snd r :: [Move]) $ \m ->
        F.for_ previous $ \p -> unless ((toRelativePoints (fst r), m) `elem` best) $ 
          refutationTable %= HM.insertWith (\x y -> take 5 $ x ++ y) p [(toRelativePoints (fst r), m)]
      return r
      
 where
  order = map snd . sortBy (comparing fst)
  states = possibleNextStates floes gs
   
  insertMove :: Move -> (a, [Move]) -> (a, [Move])
  insertMove m (p, ms)
    | gs^.turn == 8 = (p, ms)
    | otherwise = (p, m:ms)
  
-- | The 'strategy', based on 'alphaBeta' search.
strategy :: StrategyCreator (BoardFloes, GameState) Move GameUpdate
strategy gc GameUpdate (floes, gsInit) = go (SearchState HM.empty HM.empty) gsInit where
  go :: SearchState -> GameState -> Strategy Move GameUpdate
  go !table gs | gs^.turn == 8 = go table (applyMove floes gs Null)
  go !table gs = Response (go table . applyMove floes gs) (\_ -> go table gs) bestMoves
   where
    next :: (Int, (Points, Points), SearchState)
         -> ((PointsOf 'FirstPlayer, [Move]), (Int, (Points, Points), SearchState), Statistic)
    next (d, b, s)
      | fst b <= toRelativePoints points && toRelativePoints points <= snd b
          = ((points, pv), (d + 1, b', s'), stat)
      | otherwise = next (d, (minBound, maxBound), s)
     where             
      ((points, pv), s', stat) = runSearchBounded b (searchDepth gc floes gs d Nothing) s
      b' = (minBound, maxBound)

    extractMove :: ((PointsOf p, [Move]), (Int, (Points, Points), SearchState), Statistic)
                -> Maybe (String, Move, Strategy Move GameUpdate)
    extractMove ((points, moves), (_, _, s'), statistic) = case moves of
      []    -> Nothing
      (m:_) -> Just (logMessage m, m, go s' (applyMove floes gs m))
     where
      logMessage m = unwords
        [ "[" ++ show (gs^.turn) ++ "]"
        , show m
        , "[" ++ show points ++ "]"
        , showStatistic statistic
        ]

    prunedTable :: SearchState
    prunedTable = table & transpositionTable %~ HM.filterWithKey f where
     f k _ = countPositions k < 61 - gs^.turn
     -- On turn 1, there are 59 free positions. We want to keep all situations
     -- with less free positions.
     -- However, there is one turn where no extra free position is added
     -- (turn 8, NullMove), so we use 61 instead of 60 here to be save.
     

    results :: [((PointsOf 'FirstPlayer, [Move]), (Int, (Points, Points), SearchState), Statistic)]
    results = takeWhile (not . finished . view (_1._2)) . iterate (next . view _2) $
      next (1, (minBound, maxBound), prunedTable)
    {-# INLINE results #-}

    bestMoves :: [(String, Move, Strategy Move GameUpdate)]
    bestMoves = mapMaybe extractMove results
    {-# INLINE bestMoves #-}

    -- Turn 61 is the end situation,
    -- we want to keep at least one tree with that turn as leaves
    -- since we need to calculate the final score.
    -- Trees with leaves for turn > 61 don't make sense though.
    finished :: [Move] -> Bool
    finished moves = length moves + gs^.turn > 61
