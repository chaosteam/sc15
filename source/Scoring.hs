{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE RecordWildCards #-}
-- | This module provides the scoring function for game situations.
module Scoring
  ( computeScore
  ) where

import Control.Applicative
import Control.Lens
import Data.List (partition)
import Data.Monoid
import Data.Ord
import Prelude
import SC.Types

import Board
import BoardInfo
import Game
import PenguinState
import Position
import Score

import qualified Data.Foldable as F

-- | Compute a 'Score' for the given 'GameState'.
--
-- A higher score means the situation is better for us.
computeScore :: GameConfiguration -> BoardFloes -> GameState -> Points
computeScore gc floes gs
  | gs^.turn >= 61 = fromIntegral points
  | otherwise = points * 2 + boardPoints
 where
  points :: Points
  points = gs^.scores.self gc - gs^.scores.enemy gc + biasCorrection

  biasCorrection :: Points
  biasCorrection
    | gs^.turn > 8 && even (gs^.turn) = 2 -- Average score gained per turn (2 fishes)
    | otherwise = 0

  boardInfo :: BoardInfo []
  boardInfo =
    collectInfo floes (gs^.board) (playerPositions^.self gc) (playerPositions^.enemy gc)

  assigned :: BoardInfo Maybe
  assigned = assignPenguins boardInfo

  boardPoints :: Points
  boardPoints = Points $ sum
    [ componentScore floes component assigned
    | component <- availableComponents assigned
    ]

  playerPositions :: PlayerData [Position]
  playerPositions = penguinPositions <$> view penguins gs
  
-- | Compute the score of a single component.
componentScore :: BoardFloes -> ComponentID -> BoardInfo Maybe -> Int
componentScore floes component info
  | reachableEnemy == mempty && reachableSelf  /= mempty = save componentFishCount
  | reachableSelf  == mempty && reachableEnemy /= mempty = - save componentFishCount
  | otherwise = countFishes floes (reachableSelf  `removeFloes` reachableEnemy)
              - countFishes floes (reachableEnemy `removeFloes` reachableSelf )
 where
  save x = x + (x `quot` 2)
  ComponentInfo{..} = getComponentInfo component info

  reachableEnemy, reachableSelf :: PositionSet
  (reachableEnemy, reachableSelf) =
    over both (F.foldMap penguinReachable) $ partition penguinIsEnemy allPenguins

  penguinReachable :: PenguinInfo -> PositionSet
  penguinReachable = nextPositions componentPositions . penguinPosition

  allPenguins :: [PenguinInfo]
  allPenguins = map (flip getPenguinInfo info) $ getComponentPenguins component info
