{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE BangPatterns #-}
module AlphaBeta
  ( Statistic(..)
  , showStatistic
  , SearchM(), runSearchFull, evalSearchFull, runSearchBounded, alphaBeta, increaseDepth
  , withUpperBound, withLowerBound, leafStatistic, cutoffStatistic
  , Player(..), Enemy, PointsOf(), enemyPoints
  , toRelativePoints, fromRelativePoints
  , toFirstPoints, fromFirstPoints
  , toSecondPoints, fromSecondPoints
  ) where

import Control.Applicative
import Control.DeepSeq.Generics
import Control.Lens
import Control.Monad.RSS.Strict hiding ((<>))
import Data.Semigroup
import GHC.Generics (Generic)
import Prelude

import qualified Data.IntMap as M

import Score

data Statistic = Statistic
  { cutoffs        :: !(M.IntMap Int)
  , searchedLeaves :: !Int
  , minimalDepth   :: !Int
  , maximalDepth   :: !Int
  , minimalScore   :: !Points
  , maximalScore   :: !Points
  } deriving (Generic)
instance NFData Statistic where rnf = genericRnf

instance Semigroup Statistic where
  (Statistic a b c d e f) <> (Statistic a' b' c' d' e' f') =
    Statistic (M.unionWith (+) a a') (b + b') (min c c') (max d d') (min e e') (max f f')

instance Monoid Statistic where
  mempty  = Statistic M.empty 0 maxBound minBound maxBound minBound
  mappend = (<>)

showStatistic :: Statistic -> String

showStatistic statistic = concat
  [ "Leaves searched: " ++ show (searchedLeaves statistic)
  , "\t Depth: " ++ maybe "-" show (maybeMin $ minimalDepth statistic)
  , "/"          ++ maybe "-" show (maybeMax $ maximalDepth statistic)
  , "\t Score: " ++ maybe "-" show (maybeMin $ minimalScore statistic)
  , "/"          ++ maybe "-" show (maybeMax $ maximalScore statistic)
  , "\t Cutoffs: " ++ unwords (map showDepthStatistic $ M.toList $ cutoffs statistic)
  ]
 where
  showDepthStatistic (depth, count) = show depth ++ ":" ++ show count
  maybeMin x = if x == maxBound then Nothing else Just x
  maybeMax x = if x == minBound then Nothing else Just x

--------------------------------------------------------------------------------
data Player = FirstPlayer | SecondPlayer

type family Enemy p where
  Enemy 'FirstPlayer = 'SecondPlayer
  Enemy 'SecondPlayer = 'FirstPlayer

-- | Points from the view of the given player. For the given player, higher points
-- represent better situations.
newtype PointsOf (player :: Player) = PointsOf { getPointsOf :: Points }
  deriving (Generic, Bounded, Num, Real, Read, Eq, Ord, Enum, NFData)
instance Show (PointsOf player) where show (PointsOf p) = show p

-- | Get the points of a first player. This is the same as getPointsOf, but
-- safer since it actually requires the type to match.
toFirstPoints :: PointsOf 'FirstPlayer -> Points
toFirstPoints = getPointsOf

-- | Get the points of a second player.
toSecondPoints :: PointsOf 'SecondPlayer -> Points
toSecondPoints = getPointsOf

-- | Get the points relative to the current player.
toRelativePoints :: PointsOf p -> Points
toRelativePoints = getPointsOf

-- | Change between points from our view and points from the view of the enemy.
enemyPoints :: Iso' (PointsOf p) (PointsOf (Enemy p))
enemyPoints = iso (PointsOf . negate . getPointsOf) (PointsOf . negate . getPointsOf)

data LevelInfo p = LevelInfo
  { -- | The current search depth.
    _levelDepth :: Int

    -- We implement negamax, so alpha and beta scores are always computed relative to
    -- the *current player at this level*. This means that alpha will be the best score
    -- that the enemy can reach when @isEnemy@ is True and it will also be calculated
    -- from the viewpoint of the enemy, such that higher alpha values stand for a better
    -- situation *for the enemy*.
    --
    -- If @isEnemy@ is False, then alpha will represent our best score, now calculated
    -- from our point of view. So in that case, higher alpha values mean a better
    -- situation for us.
    
    -- | This is the minimum score that we are currently able to reach.
  , _levelAlpha :: PointsOf p
    
    -- | This is the maximum score that the enemy is currenlty able to reach (from
    -- our point of view).
  , _levelBeta  :: PointsOf p
  } deriving Generic
instance NFData (LevelInfo p) where rnf = genericRnf
makeLenses ''LevelInfo

newtype SearchM p s a = SearchM (RSS (LevelInfo p) Statistic s a)
  deriving (Functor, Applicative, Monad, MonadState s, MonadWriter Statistic,
            MonadReader (LevelInfo p))

getDepth :: SearchM d s Int
getDepth = view levelDepth

isEnemyTurn :: SearchM d s Bool
isEnemyTurn = odd <$> getDepth

tellStatistic :: (Int -> Statistic) -> SearchM d s ()
tellStatistic f = getDepth >>= tell . f

cutoffStatistic :: SearchM d s ()
cutoffStatistic = tellStatistic $ \depth ->
  mempty { cutoffs =  M.singleton depth 1 }

leafStatistic :: Points -> SearchM d s ()
leafStatistic score = tellStatistic $ \depth ->
  Statistic M.empty 1 depth depth score score

runSearchFull :: SearchM 'FirstPlayer s a -> s -> (a, s, Statistic)
runSearchFull = runSearchBounded (minBound, maxBound)

evalSearchFull :: SearchM 'FirstPlayer s a -> s -> a
evalSearchFull s = view _1 . runSearchFull s

runSearchBounded :: (Points, Points) -> SearchM 'FirstPlayer s a -> s -> (a, s, Statistic)
runSearchBounded (low, up) (SearchM action) = runRSS action (LevelInfo 0 low' up') where
 low' = PointsOf low
 up' = PointsOf up

-- | Run a search at increased depth. This will also change the current player.
increaseDepth :: SearchM (Enemy d) s a -> SearchM d s a
increaseDepth (SearchM a) = SearchM $ withRSS f a where
  f (LevelInfo d alpha beta) s = (LevelInfo (d + 1) alpha' beta', s) where
    alpha' = beta^.enemyPoints
    beta' = alpha^.enemyPoints

-- | Perform the given action, but only if the supplied score does not cause a cutoff.
-- If it does, the value is returned and the action is not performed.
unlessCutoff :: (PointsOf p, a) -> SearchM p s (PointsOf p, a)
             -> SearchM p s (PointsOf p, a)
unlessCutoff best action = do
  beta <- view levelBeta
  if fst best >= beta
     -- The move is so good that nobody would ever play the parent move, since there
     -- is already a better candidate (remember: beta is the minimum that the opponent
     -- can currently reach, and since best >= beta, the opponent already has a better
     -- move available).
     -- Don't bother searching for even better moves, since we don't need to know
     -- exactly how bad the move was.
     then best <$ cutoffStatistic
     else action

-- | Add a new lower bound for the our current best move. This will raise alpha if
-- the given score is higher than the current alpha.
withLowerBound :: PointsOf p -> SearchM p s a -> SearchM p s a
withLowerBound lower = local (levelAlpha %~ max lower)

-- | Add a new upper bound for the best enemy move. This will lower beta if the
-- the given score is less than the current beta.
withUpperBound :: PointsOf p -> SearchM p s a -> SearchM p s a
withUpperBound upper = local (levelBeta %~ min upper)

-- | Search the given moves, cutting where possible.
--
-- The choices should all return the score from the view of the next player, while
-- the score returned this function will be from the view of the current player.
--
-- This function returns 'Nothing' if the alpha-beta window is so narrow that no move
-- falls inside the window.
alphaBeta :: [SearchM (Enemy p) s (PointsOf (Enemy p), a)]
          -> SearchM p s (PointsOf p, a)
alphaBeta = go (minBound, error "alphaBeta: empty list")
 where
  go !best [] = return best
  go !best (c:cs) = withLowerBound (fst best) $ do
    val <- over _1 (review enemyPoints) <$> increaseDepth c
    unlessCutoff val $ go (chooseBest best val) cs

  chooseBest a b
    | fst b > fst a = b
    | otherwise = a

-- | Convert from points from the view of the first player to points from the view
-- of the current player.
fromFirstPoints :: Points -> SearchM p s (PointsOf p)
fromFirstPoints p = do
  isEnemy <- isEnemyTurn
  fromRelativePoints $ if isEnemy then -p else p

-- | Like 'fromFirstPoints', but for the second player.
fromSecondPoints :: Points -> SearchM p s (PointsOf p)
fromSecondPoints p = do
  isEnemy <- isEnemyTurn
  fromRelativePoints $ if isEnemy then p else -p

-- | Wrap points that are already from the view of the current player.
fromRelativePoints :: Points -> SearchM p s (PointsOf p)
fromRelativePoints = return . PointsOf
