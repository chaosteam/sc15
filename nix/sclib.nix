{ mkDerivation, async, base, blaze-builder, bytestring, cereal
, conduit, conduit-extra, containers, deepseq, deepseq-generics
, directory, doctest, either, ekg, errors, exceptions, fetchgit
, filepath, free, lens, mmorph, monad-loops, mtl, network
, optparse-applicative, process, resourcet, safe, semigroups
, stdenv, stm, system-time-monotonic, tagged, text, th-lift
, th-lift-instances, transformers, unix, void, xml-conduit
, xml-types
}:
mkDerivation {
  pname = "sclib";
  version = "0.1";
  src = fetchgit {
    url = "https://bitbucket.org/chaosteam/sclib";
    sha256 = "d3d216d69ff44012973be07db7af294e3c103e5942d0a9f219465214e05d2aae";
    rev = "bef60691025f48d2c92fc80168d08585b2febea6";
  };
  buildDepends = [
    async base blaze-builder bytestring cereal conduit conduit-extra
    containers deepseq deepseq-generics either ekg errors exceptions
    free lens mmorph monad-loops mtl network optparse-applicative
    process resourcet safe semigroups stm system-time-monotonic tagged
    text th-lift th-lift-instances transformers unix void xml-conduit
    xml-types
  ];
  testDepends = [ base directory doctest filepath ];
  description = "sclib";
  license = stdenv.lib.licenses.bsd3;
}
