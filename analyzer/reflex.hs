{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE RecursiveDo #-}
{-# LANGUAGE MonadComprehensions #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Main where

import Control.Concurrent
import Control.Concurrent.STM
import Control.DeepSeq
import Control.Exception
import Control.Lens
import Control.Monad
import Control.Monad.State
import Control.Monad.Supply
import Data.IORef
import Data.List (sortBy)
import Data.Maybe
import Data.Ord
import Debug.Trace hiding (traceEvent)
import Graphics.QML
import Reflex
import Reflex.Host.App
import Reflex.QML
import SC.Strategy
import SC.Types
import System.Random.Shuffle
import System.Timeout

import qualified Data.Foldable as F
import qualified Data.Set as Set
import qualified Data.Text as Text
import qualified Data.Traversable as T
import qualified Data.Vector.Unboxed as V
import qualified Reflex.QML.Prop as Prop

import AlphaBeta
import Board
import Game
import Paths_softwarechallenge15
import PenguinState
import Position
import Score
import Strategy
--------------------------------------------------------------------------------

instance Marshal PlayerColor where
  type MarshalMode PlayerColor c d = ModeTo c
  marshaller = toMarshaller $ \p -> case p of
    PlayerRed -> "red" :: Text.Text
    PlayerBlue -> "blue" :: Text.Text
--------------------------------------------------------------------------------

data History a = History
  { earlier :: [a]
  , later   :: [a]
  } deriving Eq

historyPush :: a -> History a -> History a
historyPush x (History e _) = History (x:e) []

historyNext :: History a -> Maybe (History a)
historyNext (History e (l:ls)) = Just (History (l:e) ls)
historyNext _ = Nothing

historyCurrent :: History a -> Maybe a
historyCurrent = listToMaybe . earlier

historyPrevious :: History a -> Maybe (History a)
historyPrevious (History (e:es) l) = Just (History es (e:l))
historyPrevious _ = Nothing

historyEmpty :: History a
historyEmpty = History [] []

applyMaybe :: (a -> Maybe a) -> a -> a
applyMaybe = ap fromMaybe
--------------------------------------------------------------------------------

data Worker = Worker
  { workerStatusChanged :: Bool -> IO ()
  , workerStatus :: TVar (Maybe ThreadId)
  }

createWorker :: MonadAppHost t m => Event t () -> m (Dynamic t Bool, Worker)
createWorker stopE = do
  (statusE, changeStatus) <- newExternalEvent
  workingD <- nubDyn <$> holdDyn False statusE
  worker <- liftIO $ Worker (void . changeStatus) <$> newTVarIO Nothing
  performEvent_ $ liftIO (terminateWorker worker) <$ stopE
  return (workingD, worker)

terminateWorker :: Worker -> IO ()
terminateWorker Worker{..} = do
  F.traverse_ killThread <=< atomically $
    readTVar workerStatus <* writeTVar workerStatus Nothing
  workerStatusChanged False

performEventAsyncWorker :: MonadAppHost t m => Worker -> Event t (IO a) -> m (Event t a)
performEventAsyncWorker Worker{..} taskE = do
  (resultE, fireResult) <- newExternalEvent
  performEvent_ . ffor taskE $ \action -> void . liftIO . forkIO $
   flip finally (workerStatusChanged False) $ do
    tid <- myThreadId
    F.traverse_ killThread <=< atomically $
      readTVar workerStatus <* writeTVar workerStatus (Just tid)
    workerStatusChanged True
    void $ action >>= fireResult
    atomically $ do
      status <- readTVar workerStatus
      when (status == Just tid) $ writeTVar workerStatus Nothing
  return resultE

computeDynAsyncWorker :: (NFData a, MonadAppHost t m)
                      => Worker -> Dynamic t a -> m (Dynamic t a)
computeDynAsyncWorker worker dyn = do
  newValE <- performEventAsyncWorker worker $ evaluate . force <$> updated dyn
  joinDyn <$> holdDyn dyn (constDyn <$> newValE)

--------------------------------------------------------------------------------

initialGameState :: GameState
initialGameState = GameState initialPenguins mempty initialPositions PlayerRed 0 where
  initialPenguins = PlayerData (newPenguinState []) (newPenguinState [])
  initialPositions = newPositionSet allPositions

randomBoard :: MonadIO m => m BoardFloes
randomBoard = liftIO $ do
  randomPositions <- shuffleM allPositions
  let pos1 = make OneFish $ take 30 randomPositions
      pos2 = make TwoFishes $ take 20 $ drop 30 randomPositions
      pos3 = make ThreeFishes $ drop 20 $ drop 30 randomPositions
      make f = map (flip (,) f . fromEnum)
  return $ newBoardFloes $ V.replicate 64 Sunken V.// pos1 V.// pos2 V.// pos3

performTurn :: (BoardFloes, GameState) -> Int -> IO (Maybe Move)
performTurn i maxTime = do
  let s = strategy (GameConfiguration (i^._2.nextPlayerColor) PlayerRed) GameUpdate i
  ref <- newIORef Nothing
  void . timeout maxTime . forM_ (moves s) $ \(_, m, _) -> do
    me <- evaluate m
    writeIORef ref (Just me)
  readIORef ref

computePVs :: (BoardFloes, GameState) -> Int -> [(Points, [Move])]
computePVs (floes, gs) d = moves where
  d' = max 0 d
  gc = GameConfiguration
    { selfColor = gs^.nextPlayerColor
    , startColor = PlayerRed
    }
  moves = flip evalSearchFull (SearchState mempty mempty) $ do
    forM_ [0..d'] $ \x -> searchDepth gc floes gs x Nothing
    let states = possibleNextStates floes gs
        minimax = map (over _1 toFirstPoints) . sortBy (flip $ comparing fst)
    if d' == 0
      then (:[]) . over _1 toFirstPoints <$> searchDepth gc floes gs 0 Nothing
      else fmap minimax . forM states $ \(m, gs') -> do
        (score, pv) <- increaseDepth $ searchDepth gc floes gs' (d' - 1) (Just m)
        return (enemyPoints # score, m:pv)

performDynAppHost :: MonadAppHost t m => Dynamic t (m a) -> m (Dynamic t a)
performDynAppHost dyn = holdAppHost (join . sample $ current dyn) (updated dyn)

createPosition :: MonadAppHost t m => Position -> m (Object m AnyObjRef)
createPosition pos = buildObject $ do
  Prop.constantP "column" $ pos^.coordinates._1.from enum
  Prop.constantP "row" $ pos^.coordinates._2.from enum

createMoves :: MonadAppHost t m => [Move] -> m (Object m [AnyObjRef])
createMoves ms = fmap sequence . forM ms $ \move -> do
  f <- T.traverse createPosition $ case move of
    Run p _ -> Just p
    _       -> Nothing
  t <- T.traverse createPosition $ case move of
    Run _ p  -> Just p
    Set   p  -> Just p
    Null     -> Nothing
  buildObject $ do
    Prop.constant "from" $ T.sequence f
    Prop.constant "to" $ T.sequence t

getBoardFloe :: Position -> BoardFloes -> GameState -> Floe
getBoardFloe pos floes gs = if alive then floeAt pos floes else Sunken where
  allPenguinPositions = gs^..penguins.folded.folding penguinPositions
  alive = freeFloe pos (gs^.board) || elem pos allPenguinPositions

minimalUpdate :: Ord a => [a] -> [Maybe a] -> [Maybe a]
minimalUpdate now mprevious = extend . flip runSupply input . forM previous $ \p ->
  if p `Set.member` nowSet then return (Just p) else supply
 where
  new = Set.toList $ nowSet `Set.difference` Set.fromList previous
  input = fmap Just new ++ repeat Nothing
  nowSet = Set.fromList now
  previous = catMaybes mprevious
  extend = take 4 . view both

main :: IO ()
main = mainQmlApp getDataFileName "qml/Application.qml" $ mdo
  -- General control functions
  newGameE <- Prop.methodConst "newGame" $ Prop.result . bool PlayerRed PlayerBlue
  stopWorkerE <- Prop.methodVoid "stopWorker"

  (workingD, worker) <- lift $ createWorker stopWorkerE
  Prop.readonlyP "working" workingD

  boardFloesD <- lift $ holdAppHost randomBoard (randomBoard <$ newGameE)

  -- Navigating the game: making / undoing moves
  backE <- Prop.methodVoid "back"
  forwardE <- Prop.methodVoid "forward"

  doMoveE <- Prop.methodConst "doMove" $ \mf mt -> Prop.haskellResult  $
    case Position . toEnum <$> mt of
      Just t -> maybe (Set t) (flip Run t . Position . toEnum) mf
      Nothing -> Null
  performTurnE <- Prop.methodConst "performTurn" $ \maxTime -> Prop.haskellResult $ do
    d <- sample $ current gameDataD
    return $ (,) d <$> performTurn d maxTime
  performTurnTaskE <- lift $ performEvent performTurnE
  performTurnResultE <- lift $ performEventAsyncWorker worker performTurnTaskE

  -- Computed events
  let
    moveToPerformE = push checkValidMove $ leftmost
      [ doMoveE
      , push checkStillApplicable performTurnResultE
      ]
    checkValidMove move = do
      floes <- sample $ current boardFloesD
      gs <- sample $ current gameStateD
      return [ move | move `elem` possibleMoves floes gs ]
    checkStillApplicable (d,m) = do
      d' <- sample $ current gameDataD
      return $ guard (d == d') *> m
    newGameStateE = flip pushAlways moveToPerformE $ \move -> do
      gs <- sample $ current gameStateD
      floes <- sample $ current boardFloesD
      return $ applyMove floes gs move

  -- Game data
  historyD <- foldDyn id historyEmpty $ mergeWith (.)
    [ const historyEmpty <$ newGameE
    , applyMaybe historyPrevious <$ backE
    , applyMaybe historyNext <$ forwardE
    , historyPush <$> newGameStateE
    ]
  let
    getCurrentGameState :: MonadSample t m
                        => Behavior t BoardFloes -> History GameState -> m GameState
    getCurrentGameState boardFloes history = do
      let gs = fromMaybe initialGameState $ historyCurrent history
      floes <- sample boardFloes
      return $ if gs^.turn == 8 then applyMove floes gs Null else gs
  gameStateD <- mapDynM (getCurrentGameState $ current boardFloesD) historyD
  gameDataD <- combineDyn (,) boardFloesD gameStateD
  reachableD <- forDyn gameStateD $ \gs ->
    foldMap (nextPositions (gs^.board)) . penguinPositions <$> gs^.penguins

  Prop.readonlyP "turnsBefore" =<< mapDyn (length . earlier) historyD
  Prop.readonlyP "turnsAfter" =<< mapDyn (length . later) historyD

  humanColorD <- holdDyn PlayerRed newGameE
  let computePlayerName color human =
        if color == human then "Human" else "Computer" :: Text.Text
  playerObjects <- lift $
    T.for (PlayerData PlayerRed PlayerBlue) $ \color -> buildObject $ do
      Prop.constantP "color" color
      Prop.readonlyP "name"  =<< mapDyn (computePlayerName color) humanColorD
      Prop.readonlyP "points" <=< forDyn gameStateD $
        view (scores.playerWithColor color.from enum)
      penguinPositionsD <- foldDyn minimalUpdate (replicate 4 Nothing) $
        updated gameStateD <&> views (penguins.playerWithColor color) penguinPositions
      penguinObjects <- lift . forM [0..3] $ \i -> buildObject $ do
        positionD <- nubDyn <$> mapDyn (!! i) penguinPositionsD
        Prop.readonly "position" =<< lift . performDynAppHost =<<
          mapDyn (fmap T.sequence . T.traverse createPosition) positionD
        let
          reachable Nothing (floes, gs)
            = [ p | p <- freePositions (gs^.board), floeAt p floes == OneFish ]
          reachable (Just p) (_, gs)
            = freePositions $ nextPositions (gs^.board) p

          createReachable p x = fmap sequence $ mapM createPosition (reachable p x)
        Prop.readonly "reachable" =<<
          lift . performDynAppHost =<< combineDyn createReachable positionD gameDataD
      Prop.constant "penguins" $ sequence penguinObjects
      return ()
  Prop.constant "players" . sequence . F.toList $ playerObjects

  -- Board
  let rowColumns row = (if even row then [0..6] else [0..7])
  boardCells <- lift $ forM [0..7] $ \row ->
    forM (rowColumns row) $ \col -> buildObject $ do
      let pos = coordinates # (col, row)
      Prop.constantP "boardIndex" $ fromEnum pos
      Prop.readonlyP "fishCount" =<<
        mapDyn (\(floes,gs) -> fromEnum $ getBoardFloe pos floes gs) gameDataD
      Prop.namespace "reachable" $ do
        Prop.readonlyP "red" <=< forDyn reachableD $ \reachable ->
          freeFloe pos (reachable^.red)
        Prop.readonlyP "blue" <=< forDyn reachableD $ \reachable ->
          freeFloe pos (reachable^.blue)
  boardRows <- lift $ forM boardCells $ buildObject . Prop.constant "floes" . sequence
  Prop.constant "board" $ sequence boardRows

  -- Settings for displaying additional information for analyzing
  depthD <- Prop.mutableHold "depth" (0 :: Int)
  updateMovesE <- Prop.methodVoid "updateMoves"
  nextPVE <- Prop.methodVoid "nextPV"
  previousPVE <- Prop.methodVoid "previousPV"

  let adjustToMaxPVs x = do
        pvs <- sample $ current pvsD
        return $ max 0 . min (length pvs - 1) . x
  currentPVD <- foldDyn id (0 :: Int ) $ pushAlways adjustToMaxPVs $ mergeWith (.)
    [ succ <$ nextPVE
    , pred <$ previousPVE
    ]
  Prop.readonlyP "possibility" currentPVD

  updateMovesGameDataD <- mapDynM (sample . current) =<<
    holdDyn gameDataD (gameDataD <$ updateMovesE)
  pvsD <- lift . computeDynAsyncWorker worker =<<
    combineDyn computePVs updateMovesGameDataD depthD
  movesD <- combineDyn (!!) pvsD currentPVD
  moveObjectsD <- lift $ performDynAppHost =<< mapDyn (createMoves . snd) movesD

  Prop.readonly "moves" moveObjectsD
  Prop.readonlyP "score" =<< mapDyn (fromEnum . fst) movesD

  -- Pure utility functions for QML code
  void $ Prop.methodConst "fromCoords" $ \row col ->
    Prop.pureResult . fromEnum $ coordinates # (toEnum col, toEnum row)
  void $ Prop.methodConst "turnPlayer" $ \t -> Prop.pureResult $
      if t <= (7 :: Int) && even t || t > (7 :: Int) && odd t
      then PlayerRed
      else PlayerBlue

  return ()

