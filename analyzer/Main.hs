{-# OPTIONS_GHC -fno-warn-orphans #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE RecordWildCards #-}
module Main where

import Control.Applicative
import Control.Concurrent
import Control.Concurrent.STM
import Control.Exception
import Control.Lens
import Control.Monad.Identity
import Control.Monad.State
import Data.Array.MArray
import Data.IORef
import Data.List
import Data.Maybe
import Data.Monoid
import Data.Ord
import Data.Typeable
import Graphics.QML
import Prelude
import SC.GameDefinition
import SC.Strategy
import SC.Types
import System.Random.Shuffle
import System.Timeout

import qualified Data.Foldable as F
import qualified Data.Text as T
import qualified Data.Vector.Unboxed as V

import AlphaBeta
import Board
import Game
import Paths_softwarechallenge15
import PenguinState
import Position
import Score
import Strategy

type FishTurnState = TurnState Identity Identity (BoardFloes, GameState) Move GameUpdate

data Penguin = Penguin
  { penguinPosition :: Maybe Position
  , penguinReachablePositions :: [Position]
  } deriving Eq

data PlayerState = PlayerState
  { playerColor    :: PlayerColor
  , playerName     :: T.Text
  , playerPenguins :: [TVar Penguin]
  , playerPoints   :: TVar Points
  }

data History a = History
  { earlier :: [a]
  , later   :: [a]
  } deriving Eq

historyPush :: a -> History a -> History a
historyPush x (History e _) = History (x:e) []

historyNext :: History a -> Maybe (History a)
historyNext (History e (l:ls)) = Just (History (l:e) ls)
historyNext _ = Nothing

historyCurrent :: History a -> Maybe a
historyCurrent = listToMaybe . earlier

historyPrevious :: History a -> Maybe (History a)
historyPrevious (History (e:es) l) = Just (History es (e:l))
historyPrevious _ = Nothing

historyEmpty :: History a
historyEmpty = History [] []

onResultChange :: Eq a => STM a -> IO () -> IO ()
onResultChange act handler = do
  a <- atomically act
  handler
  flip evalStateT a . forever . StateT $ \val -> join . atomically $ do
    cur <- act
    check (cur /= val)
    return $ ((), cur) <$ handler

attachThread :: ObjRef a -> IO () -> IO ()
attachThread obj act = do
  tId <- forkIO act
  flip addObjFinaliser obj =<< newObjFinaliser (\_ -> killThread tId)

data Application = Application
  { appBoard     :: TArray Position (Floe, PlayerData Bool)
  , appFloes     :: TVar BoardFloes
  , appHistory   :: TVar (History GameState)
  , appPlayers   :: TVar (PlayerData PlayerState)
  , appMoves     :: TVar [(Points, [Move])]
  , appCurrentMove :: TVar Int
  , appDepth     :: TVar Int
  , appWorker    :: TVar (Maybe ThreadId)
  } deriving Typeable

randomBoard :: IO BoardFloes
randomBoard = do
  randomPositions <- shuffleM allPositions
  let pos1 = make OneFish $ take 30 randomPositions
      pos2 = make TwoFishes $ take 20 $ drop 30 randomPositions
      pos3 = make ThreeFishes $ drop 20 $ drop 30 randomPositions
      make f = map (flip (,) f . fromEnum)
  return $ newBoardFloes $ V.replicate 64 Sunken V.// pos1 V.// pos2 V.// pos3

newApplication :: IO Application
newApplication = do
  app <- Application
    <$> atomically (newArray (minBound, maxBound) (Sunken, pure False))
    <*> (newTVarIO =<< randomBoard)
    <*> newTVarIO historyEmpty
    <*> (newTVarIO =<< makePlayers PlayerRed)
    <*> newTVarIO [(0,[])]
    <*> newTVarIO 0
    <*> newTVarIO 0
    <*> newTVarIO Nothing
  atomically $ updateGameState app
  return app

makePlayers :: PlayerColor -> IO (PlayerData PlayerState)
makePlayers humanColor = traverse create (PlayerData PlayerRed PlayerBlue) where
  create color = PlayerState color (name color)
    <$> replicateM 4 (newTVarIO unsetPenguin)
    <*> newTVarIO 0
  name color = if color == humanColor then "Human" else "Computer"
  unsetPenguin = Penguin Nothing []

showScore :: Points -> String
showScore = show . fromEnum

newApplicationObject :: Application -> IO (ObjRef Application)
newApplicationObject app@Application{..} = do
  moveChanged <- newSignalKey
  turnChanged <- newSignalKey
  depthChanged <- newSignalKey
  workerChanged <- newSignalKey
  newGameStarted <- newSignalKey
  playersRef <- newIORef =<< traverse newPlayerObject =<< readTVarIO appPlayers
  objRef <- newEmptyMVar
  obj <- flip newObject app =<< newClass
    [ boardProperty
    , defPropertySigRO' "moves" moveChanged $ \_ ->
        traverse newObjectDC <=< atomically . fmap snd $
          (!!) <$> readTVar appMoves <*> readTVar appCurrentMove
    , defPropertySigRO' "players" newGameStarted $ \_ -> F.toList <$> readIORef playersRef
    , defPropertySigRO' "turnsBefore" turnChanged $ \_ ->
        length . earlier <$> readTVarIO appHistory
    , defPropertySigRO' "turnsAfter" turnChanged $ \_ ->
        length . later <$> readTVarIO appHistory
    , defPropertySigRW' "depth" depthChanged (\_ -> readTVarIO appDepth) $
        const $ doSearch app
    , defPropertySigRO' "score" moveChanged $ \_ ->
        atomically . fmap (T.pack . showScore . fst) $
          (!!) <$> readTVar appMoves <*> readTVar appCurrentMove
    , defPropertySigRO' "possibility" moveChanged $ \_ -> readTVarIO appCurrentMove
    , defPropertySigRO' "working" workerChanged $ \_ -> isJust <$> readTVarIO appWorker
    , defMethod' "turnPlayer" $ \_ t ->
        if t <= (7 :: Int) && even t || t > (7 :: Int) && odd t
        then return PlayerRed :: IO PlayerColor
        else return PlayerBlue
    , defMethod' "updateMoves" $ \_ ->
        atomically (readTVar appDepth) >>= doSearch app
    , defMethod' "stopWorker" $ \_ ->
        atomically (readTVar appWorker <* writeTVar appWorker Nothing) >>=
        F.traverse_ killThread
    , defMethod' "doMove" $ \_ mf mt -> do
        let m = case Position . toEnum <$> mt of
              Just t -> maybe (Set t) (flip Run t . Position . toEnum) mf
              Nothing -> Null
        atomically $ do
          (floes, gs) <- appGameState app
          let valid = m `elem` possibleMoves floes gs
              gs' = applyMove floes gs m
          when valid $ modifyTVar appHistory (historyPush gs') >> updateGameState app
          return valid
    , defMethod' "back" $ \_ -> atomically $ do
        hist <- readTVar appHistory
        F.for_ (historyPrevious hist) $ \hist' ->
          writeTVar appHistory hist' >> updateGameState app
    , defMethod' "forward" $ \_ -> atomically $ do
        hist <- readTVar appHistory
        F.for_ (historyNext hist) $ \hist' ->
          writeTVar appHistory hist' >> updateGameState app
    , defMethod' "fromCoords" $ \_ row col ->
        return . fromEnum $ coordinates # (toEnum col, toEnum row) :: IO Int
    , defMethod' "nextPV" $ \_ -> atomically $ do
        ms <- readTVar appMoves
        modifyTVar appCurrentMove (min (length ms - 1) . succ)
    , defMethod' "previousPV" $ \_ -> atomically $
        modifyTVar appCurrentMove (max 0 . pred)
    , defMethod' "performTurn" $ \_ maxTime -> performTurn app maxTime
    , defMethod' "newGame" $ \_ humanIsRed -> do
        players <- makePlayers (if humanIsRed then PlayerRed else PlayerBlue)
        writeIORef playersRef =<< traverse newPlayerObject players
        floes <- randomBoard
        atomically $ do
          writeTVar appPlayers players
          writeTVar appFloes floes
          writeTVar appHistory historyEmpty
          updateGameState app
        obj <- readMVar objRef
        fireSignal newGameStarted obj :: IO ()
        return ()
    ]
  putMVar objRef obj
  attachThread obj $ readTVar appMoves `onResultChange` fireSignal moveChanged obj
  attachThread obj $ readTVar appCurrentMove `onResultChange` fireSignal moveChanged obj
  attachThread obj $ readTVar appHistory `onResultChange` fireSignal turnChanged obj
  attachThread obj $ readTVar appDepth `onResultChange` fireSignal depthChanged obj
  attachThread obj $ readTVar appWorker `onResultChange` fireSignal workerChanged obj

  return obj

newPlayerObject :: PlayerState -> IO (ObjRef ())
newPlayerObject PlayerState{..} = do
  pointsChanged <- newSignalKey
  penguinObjects <- mapM newPenguinObject playerPenguins
  obj <- flip newObject () =<< newClass
    [ defPropertyConst' "color" $ \_ -> return playerColor
    , defPropertyConst' "name"  $ \_ -> return playerName
    , defPropertyConst' "penguins" $ \_ -> return penguinObjects
    , defPropertySigRO' "points" pointsChanged $ \_ ->
       atomically $ fromEnum <$> readTVar playerPoints
    ]
  attachThread obj $ readTVar playerPoints `onResultChange` fireSignal pointsChanged obj
  return obj

newPenguinObject :: TVar Penguin -> IO (ObjRef ())
newPenguinObject penguin = do
  changed <- newSignalKey :: IO (SignalKey (IO ()))
  obj <- flip newObject () =<< newClass
    [ defPropertySigRO' "position" changed $ \_ ->
       atomically (readTVar penguin) >>= traverse newObjectDC . penguinPosition
    , defPropertySigRO' "reachable" changed $ \_ ->
       atomically (readTVar penguin) >>= traverse newObjectDC . penguinReachablePositions
    ]
  attachThread obj $ readTVar penguin `onResultChange` fireSignal changed obj
  return obj

newFloeObject :: TArray Position (Floe, PlayerData Bool) -> Int -> Int -> IO (ObjRef ())
newFloeObject b row col = do
  let pos = coordinates # (fromIntegral col, fromIntegral row)
  changed <- newSignalKey :: IO (SignalKey (IO ()))
  reachable <- flip newObject () =<< newClass
    [ defPropertySigRO' "red" changed $ const $
        view red . snd <$> atomically (readArray b pos)
    , defPropertySigRO' "blue" changed $ const $
        view blue . snd <$> atomically (readArray b pos)
    ]
  obj <- flip newObject () =<< newClass
    [ defPropertyConst' "boardIndex" $ \_ -> return (fromEnum pos)
    , defPropertyConst' "reachable" $ \_ -> return reachable
    , defPropertySigRO' "fishCount" changed $ const $
        fromEnum . fst <$> atomically (readArray b pos)
    , defSignal "changed" changed
    ]
  attachThread obj $ readArray b pos `onResultChange` do
    fireSignal changed obj
    fireSignal changed reachable
  return obj

boardProperty :: Member Application
boardProperty = defPropertyConst "board" $ \app -> do
  let b = appBoard $ fromObjRef app
  rowClass <- newClass
    [ defPropertyConst "floes" $ \rowRef -> let row = fromObjRef rowRef in
       mapM (newFloeObject b row) $ if even row then [0..6] else [0..7]
    ]
  mapM (newObject rowClass) [0..7]

instance DefaultClass Position where
  classMembers =
    [ defPropertyConst' "column" $ pure . fromEnum . view (coordinates._1) . fromObjRef
    , defPropertyConst' "row"    $ pure . fromEnum . view (coordinates._2) . fromObjRef
    ]

instance DefaultClass Move where
  classMembers =
    [ defPropertyConst' "from" $ \obj -> case fromObjRef obj of
        Null -> return Nothing
        Set _ -> return Nothing
        Run f _ -> Just <$> newObjectDC f
    , defPropertyConst' "to" $ \obj -> case fromObjRef obj of
        Null -> return Nothing
        Set p -> Just <$> newObjectDC p
        Run _ t -> Just <$> newObjectDC t
    ]

instance (Marshal a, Typeable a, CanReturnTo a ~ Yes) => DefaultClass (PlayerData a) where
  classMembers =
    [ defPropertyConst' "red" $ return . _red . fromObjRef
    , defPropertyConst' "blue" $ return . _blue . fromObjRef
    ]

instance Marshal PlayerColor where
  type MarshalMode PlayerColor c d = ModeTo c
  marshaller = toMarshaller $ \p -> case p of
    PlayerRed -> "red" :: T.Text
    PlayerBlue -> "blue" :: T.Text

mainWith :: FilePath -> Application -> IO ()
mainWith doc app = do
  context <- newApplicationObject app
  requireEventLoop . runEngine $ defaultEngineConfig
    { initialDocument = fileDocument doc
    , contextObject = Just $ anyObjRef context
    }

initialGameState :: GameState
initialGameState = GameState initialPenguins mempty initialPositions PlayerRed 0 where
  initialPenguins = PlayerData (newPenguinState []) (newPenguinState [])
  initialPositions = newPositionSet allPositions

appGameState :: Application -> STM (BoardFloes, GameState)
appGameState Application{..} = do
  history <- readTVar appHistory
  floes <- readTVar appFloes
  return (floes, maybe initialGameState (fixTurn floes) $ historyCurrent history)
 where
  fixTurn floes gs = if gs^.turn == 8 then applyMove floes gs Null else gs

updateGameState :: Application -> STM ()
updateGameState app@Application{..} = do
  (floes, gs) <- appGameState app
  let allPenguinPositions = ps^..folded.folding penguinPositions
      allReachable = foldMap (nextPositions (gs^.board)) . penguinPositions <$> ps
      reachableState pos = freeFloe pos <$> allReachable
      ps = gs^.penguins
  forM_ allPositions $ \pos ->
    if freeFloe pos (gs^.board) || elem pos allPenguinPositions
    then writeArray appBoard pos (floeAt pos floes, reachableState pos)
    else writeArray appBoard pos (Sunken, reachableState pos)
  players <- readTVar appPlayers
  forM_ [PlayerRed, PlayerBlue] $ \color -> do
    let PlayerState{..} = players^.playerWithColor color
        positions = penguinPositions $ gs^.penguins.playerWithColor color
    writeTVar playerPoints $  gs^.scores.playerWithColor color
    merge floes gs positions playerPenguins
 where
  merge floes gs as vars = do
    (as', vars') <- matchSame gs as vars
    forM_ (zip as' vars') $ \(a, var) -> writeTVar var $ Penguin (Just a) (reachable gs a)
    forM_ (drop (length as') vars') $ \var -> writeTVar var $ Penguin Nothing $
      setPositions floes gs

  matchSame _ as []   = return (as, [])
  matchSame gs as (v:vars) = do
    p <- penguinPosition <$> readTVar v
    case p of
      Just p' | (as1, _ : as2) <- break (== p') as -> do
        writeTVar v $ Penguin (Just p') (reachable gs p')
        matchSame gs (as1 ++ as2) vars
      _ -> matchSame gs as vars & mapped._2 %~ (v:)

  setPositions floes gs = [p | p <- freePositions (gs^.board), floeAt p floes == OneFish]
  reachable gs p = freePositions $ nextPositions (gs^.board) p

replaceWorker :: Application -> IO () -> IO ()
replaceWorker app@Application{..} action = do
  newWorker <- forkIO (action >> atomicFinish app (return ()))
  oldWorker <- atomically $ readTVar appWorker <* writeTVar appWorker (Just newWorker)
  F.traverse_ killThread oldWorker

atomicFinish :: Application -> STM () -> IO ()
atomicFinish Application{..} action = myThreadId >>= \selfId -> atomically $
  checkThread (/= Just selfId) <|> do
    action
    checkThread (== Just selfId)
    writeTVar appWorker Nothing
  where checkThread f = readTVar appWorker >>= check . f

performTurn :: Application -> Int -> IO ()
performTurn app@Application{..} maxTime = replaceWorker app $ do
  i <- atomically $ appGameState app
  let s = strategy (GameConfiguration (i^._2.nextPlayerColor) PlayerRed) GameUpdate i
  ref <- newIORef Nothing
  void . timeout maxTime . forM_ (moves s) $ \(_, m, _) -> do
    me <- evaluate m
    writeIORef ref (Just me)
  m <- readIORef ref
  case m of
    Nothing -> return ()
    Just m' -> atomically $ do
      i' <- appGameState app
      when (i == i') $ do
        let gs' = uncurry applyMove i m'
        modifyTVar appHistory (historyPush gs')
        updateGameState app
  return ()

doSearch :: Application -> Int -> IO ()
doSearch app@Application{..} d = replaceWorker app $ atomicFinish app $ do
  let d' = max 0 d
  (floes, gs) <- appGameState app
  let gc = GameConfiguration
        { selfColor = gs^.nextPlayerColor
        , startColor = PlayerRed
        }
      moves = flip evalSearchFull (SearchState mempty mempty) $ do
        forM_ [0..d'] $ \x -> searchDepth gc floes gs x Nothing
        let states = possibleNextStates floes gs
            minimax = map (over _1 toFirstPoints) . sortBy (flip $ comparing fst)
        if d' == 0
          then (:[]) . over _1 toFirstPoints <$> searchDepth gc floes gs 0 Nothing
          else fmap minimax . forM states $ \(m, gs') -> do
            (score, pv) <- increaseDepth $ searchDepth gc floes gs' (d' - 1) (Just m)
            return (enemyPoints # score, m:pv)
  length moves `seq` writeTVar appMoves moves
  when (null moves) $ writeTVar appMoves [(0, [])]
  writeTVar appDepth d'
  writeTVar appCurrentMove 0

startEventLoop :: IO ()
startEventLoop = do
  l <- newEmptyMVar
  void . forkIO $
    catch (runEventLoop . liftIO $ putMVar l () >> forever (threadDelay maxBound)) $
     \(_ :: EventLoopException) -> putMVar l ()
  takeMVar l

main :: IO ()
main = do
  startEventLoop
  app <- newApplication
  applicationDocument <- getDataFileName "qml/Application.qml"
  mainWith applicationDocument app
